package com.paytm.mantis;

import javax.ws.rs.client.Client;

import com.paytm.mantis.services.AdminService;
import com.paytm.mantis.services.CancellationPolicyService;
import com.paytm.mantis.services.BpDpService;
import com.paytm.mantis.services.OperatorsService;
import com.paytm.mantis.services.SearchService;
import com.paytm.mantis.services.SeatLayoutService;
import com.paytm.mantis.utilities.AerospikeManager;
import com.paytm.mantis.utilities.RestClientUtil;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MantisApplication extends Application<MantisConfiguration>
{
	public static void main(String[] args) throws Exception {
        new MantisApplication().run(args);
    }

	@Override
    public String getName() {
        return "Mantis";
    }

    @Override
    public void initialize(Bootstrap<MantisConfiguration> bootstrap) { }
	
	@Override
	public void run(MantisConfiguration configuration, Environment environment) throws Exception {
		final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration()).build("mantis-rest-client");
        final RestClientUtil restClientUtil = new RestClientUtil(client);
		
        final SearchService searchService = new SearchService(restClientUtil);
        final AdminService adminService = new AdminService(restClientUtil);
        final OperatorsService operatorsService = new OperatorsService(restClientUtil);
        final CancellationPolicyService cancellationPolicyService = new CancellationPolicyService(restClientUtil);
        final BpDpService bpDpService = new BpDpService(restClientUtil);
        final SeatLayoutService seatLayoutService = new SeatLayoutService(restClientUtil);
        
        /** Initialize aerospike */
        AerospikeManager.initializeConfiguration(configuration);
        
        /** register all services */
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.jersey().register(searchService);
        environment.jersey().register(adminService);
        environment.jersey().register(operatorsService);
        environment.jersey().register(cancellationPolicyService);
        environment.jersey().register(bpDpService);
        environment.jersey().register(seatLayoutService);
	}
}
