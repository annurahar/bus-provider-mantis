package com.paytm.mantis.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.paytm.mantis.v2.api.SearchV2Response.Bus.CancellationPolicy;
import com.paytm.mantis.v2.api.SearchV2Response.Bus.NewCancellationPolicy;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.v2.api.BaseResponse;

public interface Utility {
	Logger LOGGER = LoggerFactory.getLogger("mantis_logging");

	/**
	 * sets failure response elements
	 * @param baseResponse
	 * @return
	 */
	public static BaseResponse setFailure(BaseResponse baseResponse) {
		baseResponse.setStatusCode(Constants.STATUS_CODE_417);
		baseResponse.setResult(Constants.FAILURE);
		return baseResponse;
	}
	
	/**
	 *  return true is an array is empty
	 * @param stringArray
	 * @return
	 */
	public static boolean isEmptyArray(String[] stringArray) {
		if(stringArray==null)
			return true;
		if(stringArray!=null && stringArray.length<=0)
			return true;
		return false;
	}
	
	public static boolean isEmptyJsonArray(JSONArray jsonArray) {
		if(jsonArray==null)
			return true;
		if(jsonArray!=null && jsonArray.isEmpty())
			return true;
		return false;
	}
	
	public static boolean isNotEmptyJsonArray(JSONArray jsonArray) {
		return !isEmptyJsonArray(jsonArray);
	}
	
	/**
	 * return true is an array is not empty
	 * @param stringArray
	 * @return
	 */
	public static boolean isNotEmptyArray(String[] stringArray) {
		return !isEmptyArray(stringArray);
	}
	
	
	public static <E> boolean isEmptyList(List<E> list) {
		if(list==null)
			return true;
		if(list!=null && list.size()<=0)
			return true;
		return false;
	}
	
	
	public static <E> boolean isNotEmptyList(List<E> list) {
		return !isEmptyList(list);
	}
	
	/**
	 * This method checks weather the string is null or not
	 * @param string
	 * @return return true if string is null otherwise false
	 */
	public static boolean isEmpty(String string) {
		if(string==null)
			return true;
		else if(string!=null && string.trim().isEmpty()) {
			return true;
		}
		return false;
	}
	
	public static boolean isNotEmpty(String string) {
		return !isEmpty(string);
	}
	
	
	/** bitla returns duration is 04:00 format. Paytm format is 0400 */
	public static String getDurationInPaytmFormat(String bitlaFormatDuration) {
		String[] timeArr = bitlaFormatDuration.split(":");
		String newFormate = null;
		newFormate = timeArr[0].concat(timeArr[1]);
		return newFormate;
	}
	
	/**
	 * This method is used to split the data based on splitor
	 * @param text this is string data
	 * @param splitor this is split charchter 
	 * @return return the string array of split result
	 */
	public static String[] splitText(String text, String splitor) {
		return (text==null || text.trim().isEmpty())?null:text.split(splitor);
	}
	
	/**
	 * returns time in 24 hours format
	 * @param time - in 12 hours format
	 * @return
	 */
	public static String convertTo24HoursFormat(String time) {
		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
		Date date;
		try {
			date = parseFormat.parse(time);
			time = displayFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return time;
	}
	
	/**
	 * This method returns date format as dd-mmm-yyyy
	 * @param date - in yyyy-mm-dd format
	 * @return converted date as string
	 */
	public static String convertDate(String journeyDate) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			Date date = inputFormat.parse(journeyDate);
			journeyDate = outputFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return journeyDate;
	}
	
	/**
	 * Get a property value from a JSON object
	 * @param jsonObject
	 * @param key
	 * @return
	 */
	public static String getPropertyValue(JSONObject jsonObject, String key) {
		if (jsonObject.get(key) ==null) {			
			return "";
		}
		return  String.valueOf(jsonObject.get(key));
	}
	
	
	/**
	 * This method returns the Cancellation policy list
	 * @param jsonData get Json String for cancellation policy
	 * @return returns the list of cancellation policies
	 * 0-12|95,12-­24|80,0­-24|50,24­-48|40,48­-72|10
	 */
	public static List<NewCancellationPolicy> createNewCancellationPolicy(final String jsonData, Double minFare){
		List<NewCancellationPolicy> cancellationPolicyList = new ArrayList<>();

		if(isEmpty(jsonData)) {
			cancellationPolicyList.add(new NewCancellationPolicy(-1,0,0,0.0));
			return cancellationPolicyList;
		}
		try {
			String[] policies = (jsonData==null || jsonData.trim().isEmpty())?null:jsonData.split(",");
			if(policies!=null && policies.length>0) {
				for(int i=0;i<policies.length;i++) {
					NewCancellationPolicy cancellationPolicy = new NewCancellationPolicy();
					String[] policy =  (policies[i]==null || policies[i].trim().isEmpty())?null:policies[i].split("\\|");
	
					if(policy!=null && !isEmpty(policy[0])) {
						String[] time = (policy[0]==null || policy[0].trim().isEmpty())?null:policy[0].split("-");
						//set start time
						if(time!=null && time[0]!=null) {
							cancellationPolicy.setStartTime(Integer.parseInt(time[0].trim())*60);
						}
						//set end time
						if(time!=null && time[1]!=null) {
							cancellationPolicy.setEndTime(Integer.parseInt(time[1].trim())*60);
						}
					}
					if(policy!=null && "No Cancellation".equalsIgnoreCase(policy[1].trim())) {
						cancellationPolicy.setRefundPercentage(0);
					}else if(policy!=null && Utility.isNotEmpty(policy[1].trim()) && !policy[1].trim().contains(Constants.INR))
						cancellationPolicy.setRefundPercentage(100-Integer.parseInt(policy[1].trim()));
					
					if(policy!=null && Utility.isNotEmpty(policy[1].trim()) && policy[1].trim().contains(Constants.INR)) {
						int INRIndex = policy[1].trim().indexOf(Constants.INR);
						Double amount = Double.parseDouble(policy[1].trim().substring(0, INRIndex));
						cancellationPolicy.setRefundAmount(amount);
					}
					else
						cancellationPolicy.setRefundAmount((cancellationPolicy.getRefundPercentage() * minFare) / 100);
					cancellationPolicyList.add(cancellationPolicy);
				}
			}
		}catch(Exception exception) {
			LOGGER.error("Exception occurred ::",exception);
		}
		return cancellationPolicyList;
	}

	
	/**
	 * This method returns the New Cancellation policy list
	 * @param jsonData get Json String for cancellation policy
	 * @return returns the list of cancellation policies
	 * 0-12|95,12-­24|80,0­-24|50,24­-48|40,48­-72|10
	 */
	public static List<CancellationPolicy> createCancellationPolicy(final String jsonData) {
		List<CancellationPolicy> cancellationPolicyList = new ArrayList<>();
		LOGGER.debug("The string is :"+jsonData);
		
		if(isEmpty(jsonData)) {
			//add heading as first object if the booking is non refundable
			cancellationPolicyList.add(new CancellationPolicy("This booking is Non Refundable", ""));
			return cancellationPolicyList;
		}

		//add heading as first object
		cancellationPolicyList.add(new CancellationPolicy("Hours before Departure", 
				"Refund Percentage"));
		String[] policies = (jsonData==null || jsonData.trim().isEmpty())?null:jsonData.split(",");
		if(policies!=null && policies.length>0) {
			
			for(int i=0;i<policies.length;i++) {
				
				String[] policy = (policies[i]==null || policies[i].trim().isEmpty())?null:policies[i].split("\\|");
				
				if(policy!=null && !isEmpty(policy[0])) {
					
					String[] time = (policy[0]==null || policy[0].trim().isEmpty())?null:policy[0].split("-");
					String percentage = policy[1];
					
					String departureHeading = "Between "
							+ ""+Integer.parseInt(time[0].trim())+" Hrs. "
							+ "to "
							+""+Integer.parseInt(time[1].trim())+" Hrs. ";
					String policyHeading;
					if(percentage!=null && "No Cancellation".equalsIgnoreCase(percentage)) {
						policyHeading = "0%";
					}else
						policyHeading = 100-Integer.parseInt(percentage)+"%";
						cancellationPolicyList.add(new CancellationPolicy(departureHeading,
							policyHeading));
				}
				
			}
		}
			
		return cancellationPolicyList;
	}
	

	/**
	 * @param requestSource
	 * @param requestName
	 * @param requestId
	 * @param operatorId
	 * @param duration
	 * @param startTime
	 * @param endTime
	 * @param code
	 * @param errorId
	 */
	public static void publishMonitoringLogs(String requestSource, String requestName, String requestId, String operatorId,
			long duration, long startTime, long endTime, int code, String errorId) {
		LOGGER.info(Constants.MONITORING_LOG +Constants.MONITORING_LOG_DELIMITER
				+requestSource +Constants.MONITORING_LOG_DELIMITER
				+Constants.PROVIDER_NAME+"_"+requestName +Constants.MONITORING_LOG_DELIMITER
				+Constants.REQUEST_ID+requestId +Constants.MONITORING_LOG_DELIMITER
				+Constants.PROVIDER_BUS + Constants.MONITORING_LOG_DELIMITER
				+Constants.OPERATOR_ID+operatorId+ Constants.MONITORING_LOG_DELIMITER
				+Constants.RQ_RS_DURATION+duration +Constants.MONITORING_LOG_DELIMITER
				+Constants.START_TIME+startTime +Constants.MONITORING_LOG_DELIMITER
				+Constants.END_TIME+endTime + Constants.MONITORING_LOG_DELIMITER 
				+Constants.STATUS_CODE+code +Constants.MONITORING_LOG_DELIMITER
				+requestSource+"_"+requestName +Constants.MONITORING_LOG_DELIMITER
				+Constants.ERROR_ID+errorId);
	}
	
}
