package com.paytm.mantis.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.ScanCallback;
import com.aerospike.client.policy.BatchPolicy;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.RecordExistsAction;
import com.aerospike.client.policy.ScanPolicy;
import com.aerospike.client.policy.WritePolicy;
import com.google.gson.Gson;
import com.paytm.mantis.MantisConfiguration;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.v2.api.CancellationPolicy;
import com.paytm.mantis.v2.api.Operator;
import com.paytm.mantis.v2.api.StageDetail;

import io.dropwizard.lifecycle.Managed;

public class AerospikeManager implements Managed {
	static Logger LOGGER = LoggerFactory.getLogger("mantis_logging");
	public static List<Operator> allOperatorsList = new ArrayList<>();

	private static AerospikeManager aerospikeManager;
	private static AerospikeClient aerospikeClient;
	private static MantisConfiguration config;

	private AerospikeManager() {}
	
	public static AerospikeManager getAeroSpikeManager() {
		if(aerospikeManager==null) {
			aerospikeManager = new AerospikeManager();
		}
		return aerospikeManager;
	}
	
	public static void initializeConfiguration(MantisConfiguration mantisConfiguration) {
		config = mantisConfiguration;
		aerospikeClient = new AerospikeClient(config.getHostName(), config.getPort()); 
		/** fetch and initialize all operators list from Aerospike  */
		OperatorCallBack operatorCallBack = new OperatorCallBack();
		AerospikeManager.allOperatorsList.clear();
		allOperatorsList = fetchOperators(config.getNamespace(), config.getSetName(), operatorCallBack);
	}
	
	@Override
	public void start() throws Exception {
	}

	@Override
	public void stop() throws Exception {
		aerospikeClient.close();
	}

	/**
	 * clears and reload all operators list
	 */
	public String reloadCachedOperators() {
		OperatorCallBack operatorCallBack = new OperatorCallBack();
		 List<Operator> oldCachedOperatorsList = allOperatorsList;
		 
		AerospikeManager.allOperatorsList.clear();
		allOperatorsList = fetchOperators(config.getNamespace(), config.getSetName(), operatorCallBack);
		
		if(allOperatorsList!=null && allOperatorsList.size() > 0)
			return "Cached operators reloaded successfully.";
		
		allOperatorsList = oldCachedOperatorsList; // setting older cached operators list, if reload fails
		return "Cached operators reload failed.";
	}
	
	/**
	 * search by operatorId in cached operators List and return it
	 * @param operatorId
	 * @return
	 */
	public Operator getOperatorById(String operatorId) {
		Operator operator = null;
		for(Operator op : allOperatorsList) {
			if(!Utility.isEmpty(operatorId) && operatorId.equalsIgnoreCase(op.getOperatorId())) {
				operator = op;
				break;
			}
		}
		if(operator==null)
			LOGGER.error("No record found in Cached operator List for opeator id :: "+operatorId+" If operator is present in Aerospike, reload cached operators list.");
		else if(!operator.isEnabled())
			LOGGER.error("Operator = "+operatorId+" is currently disabled and no result will be shown for it.");
		return operator;
	}
	
	/**
	 * search by operatorId in cached cancellation Policy List and return it
	 * @param operatorId
	 * @param journeyDate 
	 * @return
	 */
	/*public CancellationPolicy getpolicyByOperator(String operatorId, String journeyDate) {
		CancellationPolicy policy = null;
		for(CancellationPolicy cPolicy : cancellationPolicyList) {
			if(Utility.isNotEmpty(operatorId) && Utility.isNotEmpty(journeyDate) 
					&& operatorId.equalsIgnoreCase(cPolicy.getOperatorId())
					&& journeyDate.equalsIgnoreCase(cPolicy.getJourneyDate())) {
				policy = cPolicy;
				break;
			}
		}
		if(policy==null)
			LOGGER.error("No record found in Cached cancellation Policy List for opeator id :: "+operatorId+" If operator is present in Aerospike, reload cached operators/policy list.");
		return policy;
	}*/
	
	
	private static List<Operator> fetchOperators(String namespace, String setName, ScanCallback callback){
		try {
			ScanPolicy policy = new ScanPolicy();
	        policy.concurrentNodes = true;
	        policy.priority = Priority.LOW;
	        policy.includeBinData = true;
	
			aerospikeClient.scanAll(policy, namespace, setName, callback);
		}catch(AerospikeException exception) {
			LOGGER.error("Aerospike scan all records query failed in fetchOperators. ",exception);
		}
		return allOperatorsList;
	}
	
	/** get all operators */
	public List<Operator> getAllOperators() {
		return allOperatorsList;
	}
	
	/**
	 * get all enabled operators
	 * @return
	 */
	public List<Operator> getAllEnabledOperators() {
		OperatorCallBack operatorCallBack = new OperatorCallBack();
        AerospikeManager.allOperatorsList.clear();
        allOperatorsList = fetchOperators(config.getNamespace(),  config.getSetName(), operatorCallBack);
		List<Operator> enabledOperators = new ArrayList<>();
		
		if(allOperatorsList!=null && allOperatorsList.size()>0) {
			for (Operator operator : allOperatorsList) {
				if(operator.isEnabled())
					enabledOperators.add(operator);
			}
		}else
			enabledOperators = null;
		return enabledOperators;
	}
	
	/**
	 * Enabled or Disable an operator
	 * @param details
	 * @return
	 */
	public Operator enableDisableOperator(Operator operator) {
		try {
			Bin isEnabledBin = new Bin("is_enabled", operator.isEnabled());
			Key key = new Key(config.getNamespace(), config.getSetName(), operator.getOperatorId());

			WritePolicy policy = new WritePolicy();
			policy.sendKey= true;
			policy.recordExistsAction = RecordExistsAction.UPDATE;
			aerospikeClient.put(policy, key, isEnabledBin);
			this.reloadCachedOperators();

		} catch (Exception exception) {
			LOGGER.error("Exception occurred in enableDisableOperator() ",exception);
		}
		return this.getOperatorById(operator.getOperatorId());
	}
	
	/**
	 * Add operator method
	 * @param details
	 * @return
	 */
	public Operator addOperator(Operator operator) {
		try {
			Bin nameBin = new Bin("name", operator.getOperatorName());
			Bin isEnabledBin = new Bin("is_enabled", operator.isEnabled());
			Bin paymentType = new Bin("payment_type", operator.getPaymentType());
			
			insertInAerospike(config.getNamespace(), config.getSetName(), operator.getOperatorId(),nameBin, isEnabledBin,paymentType);
					
		} catch (Exception exception) {
			LOGGER.error("Exception occurred in addOperator() "+exception);
		}
		return this.getOperatorById(operator.getOperatorId());
	}

	/**
	 * inserts an operator in Aerospike
	 * @param namespace
	 * @param setName
	 * @param primaryKey
	 * @param bins
	 */
	private void insertInAerospike(String namespace, String setName, String primaryKey, Bin ... bins) {
		try {
			Key key = new Key(namespace, setName, primaryKey);
			WritePolicy policy = new WritePolicy();
			policy.sendKey= true;
			policy.recordExistsAction = RecordExistsAction.REPLACE;
			aerospikeClient.put(policy, key, bins);
		}catch(Exception exception) {
			LOGGER.error("Exception occurred in insertOperator() "+exception);
		}
	}
	
	/**
	 * Updates an operator
	 * @param details
	 * @return 
	 */
	public String updateOperator(Operator newOperator) {
		String response = "Operator updated successfully.";
		try {
			Operator existingOperator = this.getOperatorById(newOperator.getOperatorId().trim());
			Bin nameBin = new Bin("name", newOperator.getOperatorName().trim());
			
			Key key = new Key(config.getNamespace(), config.getSetName(), newOperator.getOperatorId());
			WritePolicy policy = new WritePolicy();
			policy.sendKey= true;

			Bin isEnabledBin = null;
			if (existingOperator == null) { //operator doesn't exists in aerospike or cached list, so will be added
				isEnabledBin = new Bin("is_enabled", newOperator.isEnabled());
				policy.recordExistsAction = RecordExistsAction.REPLACE;
			} else {
				isEnabledBin = new Bin("is_enabled", existingOperator.isEnabled()); // in case of existing operator, update only name from operator api response
				policy.recordExistsAction = RecordExistsAction.UPDATE;
			}
			aerospikeClient.put(policy,key,nameBin,isEnabledBin);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in updateOperator() ",exception);
			response = "Error occurred while updating operator";
		}
		return response;
	}
	
	/**
	 * get cancellation policies from aerospike
	 * @param namespace
	 * @param setName
	 * @param keyList
	 * @return
	 */
	public HashMap<String, CancellationPolicy> getCancellationPolicies(ArrayList<String> keyList) {
		HashMap<String,CancellationPolicy> policiesMap = new HashMap<String,CancellationPolicy>();
		
		Record[] recordList = batchRead(config.getNamespace(), config.getCancellationPolicySetName(),keyList);
		if(recordList!=null && recordList.length > 0) {
			for(Record record : recordList) {
				if(record!=null) {
					CancellationPolicy cancellationPolicy = new CancellationPolicy();
					cancellationPolicy.setOperatorId(record.getValue("operator_id").toString());
					cancellationPolicy.setJourneyDate(record.getValue("journey_date").toString());
					cancellationPolicy.setCancellationPolicy(record.getValue("cancel_policy").toString());
					
					policiesMap.put(cancellationPolicy.getOperatorId(), cancellationPolicy);
				}
			}
		}
		return policiesMap;
	}
	
	/**
	 * batch read from aerospike using list of primary key
	 * @param namespace
	 * @param setName
	 * @param keyList
	 * @return
	 */
	private Record[] batchRead(String namespace, String setName, ArrayList<String> keyList) {
		Key[] keyArray = new Key[keyList.size()];
		BatchPolicy batchPolicy = new BatchPolicy();
		batchPolicy.sendKey=true;
		
		for(int index=0;index<keyList.size();index++)
			keyArray[index] = new Key(namespace, setName,  keyList.get(index).trim());
		
		Record[] recordList = null;
		try {
			recordList = aerospikeClient.get(batchPolicy, keyArray);
		}catch(Exception exception) {
			LOGGER.error("Exception occurred :: ",exception);
		}
		
		return recordList;
	}

	/**
	 * inserts a policy record in aerospike
	 * @param cancellationPolicyResponseList
	 * @return
	 */
	public String addPolicy(ArrayList<CancellationPolicy> cancellationPolicyResponseList) {
		String response= "Cancellation policy updation/insertion completed";
		try {
			for(CancellationPolicy policy : cancellationPolicyResponseList) {
				if(policy.getStatusCode()==Constants.STATUS_CODE_200) {
					Bin operatorBin = new Bin("operator_id", policy.getOperatorId());
					Bin dateBin = new Bin("journey_date", policy.getJourneyDate());
					Bin cancellationPoliciesBin = new Bin("cancel_policy", policy.getCancellationPolicy());
					String primaryKey = policy.getOperatorId()+"|"+policy.getJourneyDate();
					
					insertInAerospike(config.getNamespace(), config.getCancellationPolicySetName(), primaryKey,operatorBin,dateBin,cancellationPoliciesBin);
				}
			}	
		} catch (Exception exception) {
			LOGGER.error("Exception occurred in addPolicy() "+exception);
		}
		return response;
	}

	/**
	 * inserts a bpdp record in aerospike
	 * @param bpdpResponse and routeId
	 * @return
	 */
	public int addBpDpPoints(List<StageDetail> bpdppoints, String key) {
		int status = Constants.STATUS_CODE_417;
		String data = new Gson().toJson(bpdppoints);
		try {
			Bin pointdetails = new Bin("point_key", key);
			Bin bpdpresponse = new Bin("bp_dp_points", data);
			String primaryKey = key;
			insertInAerospike(config.getNamespace(), config.getBpdpSetName(), primaryKey, pointdetails, bpdpresponse);
		} catch (Exception exception) {
			LOGGER.error("Exception occurred in addPolicy() " + exception);
		}
		return status;
	}
	
	/**
	 * get bp dp response from aerospike
	 * @param keyList
	 * @return
	 */
	public HashMap<String, String> getbpdpResponse(ArrayList<String> keyList) {
		HashMap<String,String> pointsMap = new HashMap<String,String>();		
		Record[] recordList = batchRead(config.getNamespace(), config.getBpdpSetName(),keyList);
		if(recordList!=null && recordList.length > 0) {
			for(Record record : recordList) {
				if(record!=null) {				
					pointsMap.put(record.getValue("point_key").toString(), record.getValue("bp_dp_points").toString());
				}
			}
		}
		return pointsMap;
	}
	
}
