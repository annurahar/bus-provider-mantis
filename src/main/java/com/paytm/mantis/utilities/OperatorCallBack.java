package com.paytm.mantis.utilities;

import com.aerospike.client.AerospikeException;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.ScanCallback;
import com.paytm.mantis.v2.api.CancellationPolicy;
import com.paytm.mantis.v2.api.Operator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OperatorCallBack implements ScanCallback{

	@Override
	public void scanCallback(Key key, Record record) throws AerospikeException {
		Operator operator = new Operator();
		operator.setOperatorId(key.userKey.getObject().toString());
		if (record.getValue("is_enabled").toString().equals("1"))
			operator.setEnabled(true);
		operator.setOperatorName(record.getValue("name").toString());
		AerospikeManager.allOperatorsList.add(operator);
	}
}
