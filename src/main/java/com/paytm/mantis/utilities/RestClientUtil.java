package com.paytm.mantis.utilities;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpMethod;

import com.paytm.mantis.MantisConfiguration;
import com.paytm.mantis.constants.Constants;

public class RestClientUtil {

    private Client client;

    public RestClientUtil(Client client) {
        this.client = client;
    }
    

    public String getResponse(String apiPath, String entity, HttpMethod method, HashMap<String, String> queryParamNameValue){
        String result = null;
        try {
			WebTarget webTarget = client.target(MantisConfiguration.url).path(apiPath);
			
			/** Set agent id and keycode */
			webTarget = webTarget.queryParam(Constants.AGENTID, MantisConfiguration.agentId);
			webTarget = webTarget.queryParam(Constants.KEYCODE, MantisConfiguration.keyCode);
			
			if(queryParamNameValue!=null && queryParamNameValue.size()>0) {
				for (Map.Entry<String, String> entry : queryParamNameValue.entrySet()) {
					webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
				}	    	
			}
	        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
	        
	        Response response = null;
	        switch (method) {
	            case GET:
	                response = invocationBuilder.get();
	                break;
	            case POST:
	                response = invocationBuilder.post(Entity.entity(entity, MediaType.APPLICATION_JSON));
	                break;
	        }
	        result = response.readEntity(String.class);
    	}catch(Exception exception) {
    		exception.printStackTrace();
    	}
        return result;
    }
    
}
