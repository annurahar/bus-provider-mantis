package com.paytm.mantis;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

public class MantisConfiguration extends Configuration{
	@JsonProperty
    private String hostName;
	
	@JsonProperty
	private int port;
	
    @JsonProperty
    private String appName;
    
	@JsonProperty
	public static String url;

	@JsonProperty
	private String namespace;

	@JsonProperty
	private String setName;
	
	@JsonProperty
	private String cancellationPolicySetName;
	 
    @JsonProperty
    private String bpdpSetName;
    
	@JsonProperty
	public static String keyCode;
	
	@JsonProperty
	public static String agentId;
	
	@NotNull
	@JsonProperty("jerseyClient")
	private JerseyClientConfiguration jerseyClient = new JerseyClientConfiguration();

	public JerseyClientConfiguration getJerseyClientConfiguration() {
		return jerseyClient;
	}

	public void setJerseyClientConfiguration(JerseyClientConfiguration jerseyClientConfiguration) {
		this.jerseyClient = jerseyClientConfiguration;
	}

	public  String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		MantisConfiguration.url = url;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAgentId() {
		return MantisConfiguration.agentId;
	}

	public void setAgentId(String agentId) {
		MantisConfiguration.agentId = agentId;
	}

	public String getKeyCode() {
		return MantisConfiguration.keyCode;
	}

	public void setKeyCode(String keyCode) {
		MantisConfiguration.keyCode = keyCode;
	}

	public String getCancellationPolicySetName() {
		return cancellationPolicySetName;
	}

	public void setCancellationPolicySetName(String cancellationPolicySetName) {
		this.cancellationPolicySetName = cancellationPolicySetName;
	}

	public String getBpdpSetName() {
		return bpdpSetName;
	}

	public void setBpdpSetName(String bpdpSetName) {
		this.bpdpSetName = bpdpSetName;
	}

	
}
