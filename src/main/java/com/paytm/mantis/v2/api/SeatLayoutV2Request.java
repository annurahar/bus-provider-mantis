package com.paytm.mantis.v2.api;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class SeatLayoutV2Request extends BaseRequest {
	@JsonProperty("source_id")
	public String sourceId;

	@JsonProperty("destination_id")
	public String destinationId;

	@JsonProperty("journey_date")
	public String journeyDate;

	@NotNull
	@JsonProperty("trip_id")
	public String tripId;

	@JsonProperty("operator_id")
	public String operatorId;
}
