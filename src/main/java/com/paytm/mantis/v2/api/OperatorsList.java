package com.paytm.mantis.v2.api;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OperatorsList extends BaseResponse{
	@JsonProperty("operators")
	List<Operator> allOpeators;
}
