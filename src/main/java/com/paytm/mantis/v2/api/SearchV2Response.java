package com.paytm.mantis.v2.api;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Hold the json response of search data in java object
 *
 */
@Data
@JsonPropertyOrder({"info","body"})
public class SearchV2Response extends BaseResponse {
	private List<Bus> body;

	@Data
	@JsonPropertyOrder({"info", "source_id", "destination_id", "bus_start_source_id",
		"bus_end_destination_id", "collect_boarding_point", "is_concession_applicable", "concession",
		"trip_id", "operator_id", "service_id", "service_code", "service_category_id",
		"service_category_name", "route_id", "class_id", "arrival_datetime",
		"departure_datetime", "duration", "total_seats", "raw_fares", "available_seats", "is_ac", "is_sleeper",
		"is_semi_sleeper", "is_seater", "is_m_ticket_allowed", "single_lady_feature", "bus_name",
		"bus_type_name", "layout_type", "bus_class_type", "axle_type", "partial_cancellation_allowed",
		"fare", "is_tax_inclusive", "id_proof_required", "id_proof_type", "old_cancellation_policy", "boarding_points",
		"dropping_points", "is_live_tracking_available", "amenities", "is_refundable",
		"raw_cancellation_policy", "cancellation_policy", "distance", "bus_route", "rest_points",
		"photos"})
	public static class Bus {
		private Info info;
		@JsonProperty("service_id")
		private String serviceId;
		@JsonProperty("source_id")
		private String sourceId;
		@JsonProperty("service_code")
		private String serviceCode;
		@JsonProperty("destination_id")
		private String destinationId;
		@JsonProperty("route_id")
		private String routeId;
		@JsonProperty("class_id")
		private String classId;
		@JsonProperty("bus_start_source_id")
		private String busStartSourceId;
		@JsonProperty("bus_end_destination_id")
		private String busEndDestinationId;
		private boolean collectBoardingPoint;
		private boolean isConcessionApplicable;
		private List<Concession> concession;
		@JsonProperty("trip_id")
		private String tripId;
		@JsonProperty("operator_id")
		private String operatorId;
		@JsonProperty("service_category_id")
		private String serviceCategoryId;
		@JsonProperty("service_category_name")
		private String serviceCategoryName;
		@JsonProperty("arrival_datetime")
		private String arrivalDateTime;
		@JsonProperty("departure_datetime")
		private String departureDateTime;
		private String duration;
		@JsonProperty("total_seats")
		private int totalSeats;
		@JsonProperty("raw_fares")
		private String rawFares;
		@JsonProperty("available_seats")
		private int availableSeats;
		@JsonProperty("bus_name")
		private String busName;
		private boolean isAc;
		private boolean isSleeper;
		private boolean isSemiSleeper;
		private boolean isSeater;
		private boolean isMTicketAllowed;
		private boolean singleLadyFeature;
		@JsonProperty("bus_type_name")
		private String busTypeName;
		@JsonProperty("layout_type")
		private String layoutType;
		@JsonProperty("bus_class_type")
		private String busClassType;
		@JsonProperty("axle_type")
		private String axleType;
		private boolean partialCancellationAllowed;
		private List<Double> fare;
		private boolean isTaxInclusive;
		@JsonProperty("boarding_points")
		private List<StageDetail> boardingPoints;
		@JsonProperty("dropping_points")
		private List<StageDetail> droppingPoints;
		private boolean isLiveTrackingAvailable;
		@JsonProperty("amenities")
		private List<Amenity> amenities;
		private boolean isRefundable;
		@JsonProperty("raw_cancellation_policy")
		private List<String> rawCancellationPolicy;
		@JsonProperty("cancellation_policy")
		private List<NewCancellationPolicy> cancellationPolicy;
		private long distance;
		@JsonProperty("bus_route")
		private List<String> busRoute;
		@JsonProperty("rest_points")
		private List<RestPoint> restPoints;
		private List<String> photos;
		private boolean idProofRequired;
		private boolean idProofType;
		@JsonProperty("old_cancellation_policy")
		private List<CancellationPolicy> oldCancellationPolicy;
		
		@JsonProperty("collect_boarding_point")
		public void setCollectBoardingPoint(boolean collectBoardingPoint) {
			this.collectBoardingPoint = collectBoardingPoint;
		}
		@JsonProperty("is_concession_applicable")
		public void setConcessionApplicable(boolean isConcessionApplicable) {
			this.isConcessionApplicable = isConcessionApplicable;
		}
		@JsonProperty("is_ac")
		public void setAc(boolean isAc) {
			this.isAc = isAc;
		}
		@JsonProperty("is_sleeper")
		public void setSleeper(boolean isSleeper) {
			this.isSleeper = isSleeper;
		}
		@JsonProperty("is_semi_sleeper")
		public void setSemiSleeper(boolean isSemiSleeper) {
			this.isSemiSleeper = isSemiSleeper;
		}
		@JsonProperty("is_seater")
		public void setSeater(boolean isSeater) {
			this.isSeater = isSeater;
		}
		@JsonProperty("is_m_ticket_allowed")
		public void setMTicketAllowed(boolean isMTicketAllowed) {
			this.isMTicketAllowed = isMTicketAllowed;
		}
		@JsonProperty("single_lady_feature")
		public void setSingleLadyFeature(boolean singleLadyFeature) {
			this.singleLadyFeature = singleLadyFeature;
		}
		@JsonProperty("partial_cancellation_allowed")
		public void setPartialCancellationAllowed(boolean partialCancellationAllowed) {
			this.partialCancellationAllowed = partialCancellationAllowed;
		}
		@JsonProperty("is_tax_inclusive")
		public void setTaxInclusive(boolean isTaxInclusive) {
			this.isTaxInclusive = isTaxInclusive;
		}
		@JsonProperty("is_refundable")
		public void setRefundable(boolean isRefundable) {
			this.isRefundable = isRefundable;
		}
		@JsonProperty("is_live_tracking_available")
		public void setLiveTrackingAvailable(boolean isLiveTrackingAvailable) {
			this.isLiveTrackingAvailable = isLiveTrackingAvailable;
		}
		@JsonProperty("id_proof_required")
		public void setIdProofRequired(boolean idProofRequired) {
			this.idProofRequired = idProofRequired;
		}
		@JsonProperty("id_proof_type")
		public void setIdProofType(boolean idProofType) {
			this.idProofType = idProofType;
		}
		
		
		@Data
		@JsonPropertyOrder({ "id", "type", "name", "discount_percent", "max_seats",
			"is_user_enabled"})
		public static class Concession{
			private String id;
			private String type;
			private String name;
			@JsonProperty("discount_percent")
			private double discountPercent;
			@JsonProperty("max_seats")
			private int maxSeats = 5;
			private boolean isUserEnabled;
			@JsonProperty("boarding_points")
			public void setUserEnabled(boolean isUserEnabled) {
				this.isUserEnabled=isUserEnabled;
			}
		}
		@Data
		public static class Amenity{
			private String name;
			private String description;
		}
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@JsonPropertyOrder({ "start_time", "end_time", "refund_percentage", "refund_amount"})
		public static class NewCancellationPolicy{
			@JsonProperty("start_time")
			private int startTime;
			@JsonProperty("end_time")
			private int endTime;
			@JsonProperty("refund_percentage")
			private int refundPercentage;
			@JsonProperty("refund_amount")
			private Double refundAmount;
		}
		@Data
		@JsonPropertyOrder({ "id", "name", "address", "photo", "stop_datetime",
			"stop_duration", "latitude", "longitude"})
		public static class RestPoint{
			private String id;
			private String name;
			private String address;
			private List<String> photos;
			@JsonProperty("stop_datetime")
			private String stopDateTime;
			@JsonProperty("stop_duration")
			private String stopDuration;
			private String latitude;
			private String longitude;
		}
		
		@Data
		@JsonPropertyOrder({"scheduleId", "scheduleNumber", "scheduleName", "sourceStationId", 
			"destinationStationId", "operatorServiceName", "routeId", "travelId", "busType",
			"fare", "providerFareFormat", "availableSeats", "totalSeats", "status", "opScheduleId", 
			"tripId", "journeyDate", "cancellationPolicyJSON", "new_cancellation_policy", 
			"cancellation_policy_note_text"})
		public static class Info{
			private String scheduleId;
			private String scheduleNumber;
			private String scheduleName;
			private String sourceStationId;
			private String destinationStationId;
			private String operatorServiceName;
			private String routeId;
			private String travelId;
			private String busType;
			private List<Double> fare;
			private String providerFareFormat;
			private String availableSeats;
			private String totalSeats;
			private String status;
			private String opScheduleId;
			private String tripId;
			private String journeyDate;
			private List<CancellationPolicy> cancellationPolicyJSON;
			@JsonProperty("new_cancellation_policy")
			private List<NewCancellationPolicy> newCancellationPolicy; 
			@JsonProperty("cancellation_policy_note_text")
			private String cancellationPolicyNoteText;
		
		
		}
		
		@Data
		@NoArgsConstructor
		@AllArgsConstructor
		@JsonPropertyOrder({"departure_heading","policy_heading"})
		public static class CancellationPolicy{
			@JsonProperty("departure_heading")
			String departureHeading;
			@JsonProperty("policy_heading")
			String policyHeading;
		}
		
	
		
	}
}
