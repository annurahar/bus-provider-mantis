package com.paytm.mantis.v2.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BaseRequest {
	protected ApiMeta meta;
	
	@Data
	public static class ApiMeta {
		@JsonProperty("request_id")
		protected String requestId;
		
		@JsonProperty("provider_name")
		protected String providerName = "bitla_direct";
		
		@JsonProperty("provider_id")
		protected int providerId = 39;
		
		protected String version;
		protected String client;
		
		@JsonProperty("time_stamp")
		protected String timeStamp;
	}
}
