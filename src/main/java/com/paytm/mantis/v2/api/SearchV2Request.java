package com.paytm.mantis.v2.api;


import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class SearchV2Request extends BaseRequest{
	@NotNull
	@JsonProperty("source_id")
	public String sourceId;

	@NotNull
	@JsonProperty("destination_id")
	public String destinationId;

	@NotNull
	@JsonProperty("journey_date")
	public String journeyDate;
}
