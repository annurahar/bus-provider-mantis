package com.paytm.mantis.v2.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
/**
 * This is the Parent class of each response classes which is used to get the response
 *
 */
@Data
@JsonPropertyOrder({"status_code", "result","error_category","error_internal","error_code","error_id","error"})
public abstract class BaseResponse {
	@JsonProperty("status_code")
	private int statusCode;

	@JsonProperty("result")
	private String result="SUCCESS";
	
	@JsonProperty("error_category")
	private String errorCategory;
	
	@JsonProperty("error_internal")
	private String errorInternal;
	
	@JsonProperty("error_code")
	private String errorCode;
	
	@JsonProperty("error_id")
	private String errorid;
	
	@JsonProperty("error")
	private BaseResponseError error;
	
	@Data
	@JsonPropertyOrder({"internal", "category", "code"})
	public static class BaseResponseError{
		@JsonProperty("internal")
		private String internal;
		@JsonProperty("category")
		private String category;
		@JsonProperty("code")
		private String code;
	}
	
}
