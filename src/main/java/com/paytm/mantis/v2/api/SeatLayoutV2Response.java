package com.paytm.mantis.v2.api;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"seats", "collect_pax_id", "boarding_points", "dropping_points",
	"fare_breakup_list"})
public class SeatLayoutV2Response extends BaseResponse {

	@JsonProperty("boarding_points")
	private List<StageDetail> boardingPoints;
	@JsonProperty("dropping_points")
	private List<StageDetail> droppingPoints;
	@JsonProperty("collect_pax_id")
	private boolean collectPaxid;
	private List<Seats> seats;
	@JsonProperty("fare_breakup_list")
	private List<FareBreakup> fareBreakupList;
	
	@Data
	@JsonPropertyOrder({"seat_code", "type", "column", "row", "is_available", "fare",
		"is_tax_inclusive", "fare_breakup_type", "is_reserved_for_ladies", "is_reserved_for_men",
		 "is_lower_berth", "length", "width", "seat_number", "is_reserved_for_senior_citizen"})
	public static class Seats{
		@JsonProperty("seat_code")
		private String seatCode;
		private String type;
		private int column;
		private int row;
		private boolean isAvailable;
//		Adding fare = 0, as default value
		private double fare = 0;
		private boolean isTaxInclusive;
		@JsonProperty("fare_breakup_type")
		private int fareBreakupType;
		private boolean isReservedForLadies;
		private boolean isReservedForMen;
		private boolean isLowerBerth;
		private int length = 1; // By default set this to 1.
		private int width = 1; //This is always 1.
		@JsonProperty("seat_number")
		private String seatNumber;
		@JsonProperty("max_seats_allowed")
		private int maxAllowedSeat=5;
		private boolean isReservedForSeniorCitizen;
		
		@JsonProperty("is_available")
		public void setAvailable(boolean isAvailable) {
			this.isAvailable=isAvailable;
		}
		@JsonProperty("is_tax_inclusive")
		public void setTaxInclusive(boolean isTaxInclusive) {
			this.isTaxInclusive=isTaxInclusive;
		}
		@JsonProperty("is_reserved_for_ladies")
		public void setReservedForLadies(boolean isReservedForLadies) {
			this.isReservedForLadies=isReservedForLadies;
		}
		@JsonProperty("is_reserved_for_men")
		public void setReservedForMen(boolean isReservedForMen) {
			this.isReservedForMen=isReservedForMen;
		}
		@JsonProperty("is_lower_berth")
		public void setLowerBerth(boolean isLowerBerth) {
			this.isLowerBerth=isLowerBerth;
		}
		@JsonProperty("is_reserved_for_senior_citizen")
		public void setReservedForSeniorCitizen(boolean isReservedForSeniorCitizen) {
			this.isReservedForSeniorCitizen=isReservedForSeniorCitizen;
		}
	}
	
	public static enum Type{
		SS, S, SL;
		Type(){}
	}
	
}
