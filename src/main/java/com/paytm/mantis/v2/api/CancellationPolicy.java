package com.paytm.mantis.v2.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CancellationPolicy extends BaseResponse {
	@JsonProperty("cancellation_policy")
	String cancellationPolicy;
	
	@JsonProperty("operator_id")
	String operatorId;
	
	@JsonProperty("journey_date")
	String journeyDate;
	
}
