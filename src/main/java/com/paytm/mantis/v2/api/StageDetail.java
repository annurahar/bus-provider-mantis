package com.paytm.mantis.v2.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonPropertyOrder({ "id", "name", "date_time", "address",
"contact_number", "latitude", "longitude", "platform_number", "landmark"})
public class StageDetail {
	private String id;
	private String name;
	@JsonProperty("datetime")
	private String dateTime;
	private String address;
	@JsonProperty("contact_number")
	private List<String>  contactNumber;
	private String latitude;
	private String longitude;
	@JsonProperty("platform_number")
	private String platformNumber;
	private String landmark;
	
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setContactNumber(List<String> contactNumber) {
		this.contactNumber = contactNumber;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public void setPlatformNumber(String platformNumber) {
		this.platformNumber = platformNumber;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getId() {
		return id;
	}
	public String getDateTime() {
		return dateTime;
	}
	
	
}