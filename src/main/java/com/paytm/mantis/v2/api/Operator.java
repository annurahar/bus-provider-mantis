package com.paytm.mantis.v2.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Operator{
	@JsonProperty("is_enabled")
	private boolean enabled;

	@JsonProperty("operator_id")
	private String operatorId;

	@JsonProperty("operator_name")
	private String operatorName;
	
	@JsonProperty("payment_type")
	private String paymentType;
	
}
