package com.paytm.mantis.v2.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonPropertyOrder({"base_fare", "gst", "service_tax", "luxury_tax", "toll_fee",
	 "reservation_fee", "levy_fee", "service_fee", "bank_transaction_fee", "concession",
	  "round_trip_discount", "total_fare"})
public class FareBreakup {
	@JsonProperty("base_fare")
	private double baseFare;
	private double gst;
	@JsonProperty("service_tax")
	private double serviceTax;
	@JsonProperty("luxury_tax")
	private double luxuryTax;
	@JsonProperty("toll_fee")
	private double tollFee;
	@JsonProperty("reservation_fee")
	private double reservationFee;
	@JsonProperty("levy_fee")
	private double levyFee;
	@JsonProperty("service_fee")
	private double serviceFee;
	@JsonProperty("bank_transaction_fee")
	private double bankTransactionFee;
	private double concession;
	@JsonProperty("round_trip_discount")
	private double roundTripDiscount;
	@JsonProperty("total_fare")
	private double totalFare;

}

