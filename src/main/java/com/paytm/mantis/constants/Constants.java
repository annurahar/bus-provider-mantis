package com.paytm.mantis.constants;

import java.util.HashMap;
import java.util.Map;

public class Constants {
	
	//Common
	public static final String AGENTID = "AgentID";
	public static final String KEYCODE = "KeyCode";
	public static final String FROMCITYID = "FromCityID";
	public static final String TOCITYID = "ToCityID";
	public static final String JOURNEYDATE = "JourneyDate";
	public static final String ROUTEID = "RouteCode";
	
	public static final String PROVIDER_BUS = "provider_bus";
	public static final int STATUS_CODE_200 = 200;
	public static final int STATUS_CODE_417 = 417;
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILURE = "FAILURE";
	public static final String ENABLED = "1";
	
	public static final String MONITORING_LOG = "MONITORING_LOG";
	public static final String MONITORING_LOG_DELIMITER = " # ";
	public static final String IMS = "IMS";
	public static final String TRAVELYAARI = "TRAVELYAARI";
	public static final String REQUEST_ID = "requestId:";
	public static final String OPERATOR_ID = "operatorId:";
	public static final String RQ_RS_DURATION = "duration:";
	public static final String START_TIME = "startTime:";
	public static final String END_TIME = "endTime:";
	public static final String STATUS_CODE = "statusCode:";
	public static final String PROVIDER_NAME="travelyaari";
	public static final String ERROR_ID="errorId:";
	
	/**Mantis Search response keys*/	
	public static final String SEARCH = "search";
	public static final String APISEARCHRESPONSE = "APISearchRoutesV2Response";
	public static final String APISEARCHRESULT = "APISearchRoutesV2Result";
	public static final String ROUTE = "a:RouteV2";
	public static final String FORMCITYID = "a:FromCityID";
	public static final String ROUTENAME = "a:RouteName";
	public static final String CHARTDATE = "a:ChartDate";
	public static final String DEPARTURETIME = "a:DepartureTime";
	public static final String ISAC = "a:HasAC";
	public static final String BUSTYPE = "a:BusType";
	public static final String COMPANYNAME = "a:CompanyName";
	public static final String SLEEPERFARE = "a:LowestSleeperFare";
	public static final String SEATERFARE = "a:LowestSeaterFare";
	public static final String ARRIVALTIME = "a:ArrivalTime";
	public static final String TRIPID = "a:TripID";
	public static final String COMPANYID = "a:CompanyID";
	public static final String HASAC = "a:HasAC";
	public static final String HASSEATER= "a:HasSeaters";
	public static final String HASLEEPER= "a:HasSleepers";
	public static final String ROUTECODE = "a:RouteCode";
	public static final String SERVICEID = "a:ServiceID";
	public static final String AVAILABILITY = "a:Availability";
	public static final String HASTAX = "a:HasSTax";
	public static final String SUFFIX = "a:Suffix";
	public static final String LOWESTSEATERFARE = "a:LowestSeaterFare";
	public static final String LOWESTSLEEPERFARE ="a:LowestSleeperFare";
	public static final String APIPICKUPDROPOFF = "APIGetPickupsDropoffsResponse";
	public static final String APIPICKUPDROPOFFRESULT = "APIGetPickupsDropoffsResult";
	public static final String ISNONREFUNDABLE = "a:IsNonRefundable";
	public static final String DROPOFFS = "a:Dropoffs";
	public static final String DROPOFF = "a:Dropoff";
	public static final String PICKUPS = "a:Pickups";
	public static final String PICKUP = "a:Pickup";
	public static final String PICKUPID="a:PickupID";
	public static final String PICKUPNAME="a:PickupName";
	public static final String Address="a:Address";
	public static final String PICKUPTIME="a:PickupTime";
	public static final String PICKUPCONTACTS ="a:ContactNumbers";
	public static final String DROPUPID = "a:DropoffID";
	public static final String DROPNAME = "a:DropoffName";
	public static final String DROPUPTIME ="a:DropoffTime";
	public static final String PICKUPLANDMARK="a:Landmark";
	public static final String ID="id";
	public static final String NAME="name";
	public static final String ADDRESS="address";
	public static final String DATETIME="dateTime";
	public static final String LANDMARK="landmark";
	public static final String CONTACTNUMBER="contactNumber";
	
	
	//Opeartor/Company list
	public static final String APIGETCOMPANIESRESPONSE = "APIGetCompaniesResponse";
	public static final String APIGETCOMPANIESRESULT = "APIGetCompaniesResult";
	public static final String COMPANY = "a:Company";
	public static final String COMPANY_ID = "CompanyID";
	
	//Cancellation Policies
	public static final String APIGETCANCELLATIONPOLICIESRESPONSE = "APIGetCancellationPoliciesResponse";
	public static final String APIGETCANCELLATIONPOLICIESRESULT = "APIGetCancellationPoliciesResult";
	public static final String CANCELLATIONPOLICY = "a:CancellationPolicy";
	public static final String CHARGEAMT = "a:ChargeAmt";
	public static final String CHARGEPCT = "a:ChargePCT";
	public static final String CHARTPCT = "a:ChartPCT";
	public static final String MINSBEFORE = "a:MinsBefore";
	public static final String INR = "-INR";
	
	//Seat layout
	public static final String ROUTE_CODE = "RouteCode";
	public static final String SEAT_LAYOUT = "seat_layout";
	public static final String APIGetChartResponse = "APIGetChartResponse";
	public static final String APIGetChartResult = "APIGetChartResult";
	public static final String Seat = "a:Seat";
	public static final String ColumnNo = "a:ColumnNo";
	public static final String CompanyChartID = "a:CompanyChartID";
	public static final String CompanyChartName ="a:CompanyChartName";
	public static final String Deck = "a:Deck";
	public static final String Fare = "a:Fare";
	public static final String Gender = "a:Gender";
	public static final String HasSCharge = "a:HasSCharge";
	public static final String HasSTax = "a:HasSTax";
	public static final String Height = "a:Height";
	public static final String IsAC = "a:IsAC";
	public static final String IsAisle = "a:IsAisle";
	public static final String IsAvailable = "a:IsAvailable";
	public static final String RouteCode = "a:RouteCode";
	public static final String RowNo = "a:RowNo";
	public static final String SeatLabel = "a:SeatLabel";
	public static final String ServiceCharge = "a:ServiceCharge";
	public static final String ServiceTax = "a:ServiceTax";
	public static final String Width = "a:Width";
			
	
	/** IMS Block api error details */
	public static Map<String, String> IMS_BLOCK_ERROR_CATEGORY_MAP = new HashMap<>();
	static {
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EIMSFAIL39","provider_service_failed"); 
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATRES39","seat_unavailable"); 
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EFAREMISMATCH39","trip_unavailable"); 
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EPROBLOCK39","provider_block_failed"); 
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATFDS39","gender_check_failed"); 
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATDS39","single_seat_booking_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATMF39","gender_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATF39","gender_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ELBAL39","low_balance");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATFM39","gender_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EUSER39","provider_bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EDEPT39","trip_unavailable");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EPARAM39","provider_bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EFAREMISMATCH39","provider_bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESERVC39","trip_unavailable");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EAGECHK39","age_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EAGECHK39","age_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ECHLDTRVL39","age_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ETIMEOUT39","timeout");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EPARAM39","bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EAUTH39","unauthorised");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATFM39","gender_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ETWOSINGLE39","provider_block_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EGENDER39","gender_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EMISSID39","id_card_missing");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EWINDOW39","incorrect_search_window");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATCH39","seat_unavailable");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATX39","provider_maintenance");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ELASTF39","gender_check_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATDB39","provider_block_failed");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ESEATMAX39","provider_bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EPAXZERO39","provider_bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EBPINVALID39","provider_bad_request");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("EROUTX39","provider_maintenance");
		IMS_BLOCK_ERROR_CATEGORY_MAP.put("ETIMECH39","provider_block_failed");
	}
	
	public static Map<String, String> IMS_BLOCK_ERROR_CODE_MAP = new HashMap<>();
	static {
		IMS_BLOCK_ERROR_CODE_MAP.put("EIMSFAIL39","block_failed"); 
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATRES39","seat_already_reserved"); 
		IMS_BLOCK_ERROR_CODE_MAP.put("EFAREMISMATCH39","service_cancelled"); 
		IMS_BLOCK_ERROR_CODE_MAP.put("EPROBLOCK39","unable_to_block_seats"); 
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATFDS39","single_female_seat_type"); 
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATDS39","single_seat_booking_failed");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATMF39","male_next_to_female");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATF39","unreserved_female_seat");
		IMS_BLOCK_ERROR_CODE_MAP.put("ELBAL39","low_balance");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATFM39","female_next_to_male");
		IMS_BLOCK_ERROR_CODE_MAP.put("EUSER39","invalid_user");
		IMS_BLOCK_ERROR_CODE_MAP.put("EDEPT39","bus_departed");
		IMS_BLOCK_ERROR_CODE_MAP.put("EPARAM39","incorrect_request_parameter");
		IMS_BLOCK_ERROR_CODE_MAP.put("EFAREMISMATCH39","fare_mismatch");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESERVC39","service_cancelled");
		IMS_BLOCK_ERROR_CODE_MAP.put("EAGECHK39","senior_citizen_65");
		IMS_BLOCK_ERROR_CODE_MAP.put("EAGECHK39","senior_citizen_60");
		IMS_BLOCK_ERROR_CODE_MAP.put("ECHLDTRVL39","child_without_adult");
		IMS_BLOCK_ERROR_CODE_MAP.put("ETIMEOUT39","request_timeout");
		IMS_BLOCK_ERROR_CODE_MAP.put("EPARAM39","invalid_parameters");
		IMS_BLOCK_ERROR_CODE_MAP.put("EAUTH39","invalid_user");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATFM39","reserved_female_seat");
		IMS_BLOCK_ERROR_CODE_MAP.put("ETWOSINGLE39","two_single_seats_apart");
		IMS_BLOCK_ERROR_CODE_MAP.put("EGENDER39","gender_restriction");
		IMS_BLOCK_ERROR_CODE_MAP.put("EMISSID39","id_card_missing");
		IMS_BLOCK_ERROR_CODE_MAP.put("EWINDOW39","out_of_search_window");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATCH39","seat_arrangement_changed");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATX39","seat_transfer_in_progress");
		IMS_BLOCK_ERROR_CODE_MAP.put("ELASTF39","female_seat_last_row");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATDB39","single_seat_double_berth");
		IMS_BLOCK_ERROR_CODE_MAP.put("ESEATMAX39","max_seat_limit_excedded");
		IMS_BLOCK_ERROR_CODE_MAP.put("EPAXZERO39","zero_passenger");
		IMS_BLOCK_ERROR_CODE_MAP.put("EBPINVALID39","incorrect_boarding_point");
		IMS_BLOCK_ERROR_CODE_MAP.put("EROUTX39","route_transfer_in_progress");
		IMS_BLOCK_ERROR_CODE_MAP.put("ETIMECH39","time_mismatch");
	}
	
	public static Map<String, String> IMS_BLOCK_ERROR_INTERNAL_MAP = new HashMap<>();
	static {
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EIMSFAIL39","Others"); 
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATRES39","Seat Already Blocked or Booked"); 
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EFAREMISMATCH39","Service has been cancelled"); 
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EPROBLOCK39","Provider is unable to block seats"); 
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATFDS39","Single Female cannot book seats in double sleeper"); 
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATDS39","Single person cannot book seats in double sleeper");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATMF39","Male cannot book next to female");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATF39","Female only can book female reserved seats");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ELBAL39","Balance Low");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATFM39","Female cannot book next to male");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EUSER39","Invalid User");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EDEPT39","Bus Booking Time is Over");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EPARAM39","Incorrect Request Parameters");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EFAREMISMATCH39","Fare Incorrect");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESERVC39","Route Unavailable");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EAGECHK39","Senior Citizen Age cannot be less than X");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EAGECHK39","Senior Citizen Age cannot be less than X");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ECHLDTRVL39","Child Cannot Travel Alone");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ETIMEOUT39","Timeout");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EPARAM39","Bad Request");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EAUTH39","Authentication Error");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATFM39","Male cannot book ladies reserved seat");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ETWOSINGLE39","Two single seats cannot be booked apart");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EGENDER39","Gender restriction");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EMISSID39","ID card details missing for senior citizen");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EWINDOW39","Out of maximum advance booking date");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATCH39","Seat Arrangement has been changed");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATX39","Arrangement Transfer is in progress");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ELASTF39","Cannot Book Female seat in last row");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATDB39","Cannot Book Single Seat in Double Berth");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ESEATMAX39","Maximun seat limit exceeded");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EPAXZERO39","Passenger count cannot be zero");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EBPINVALID39","Pick up is not available");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("EROUTX39","Route Transfer is in prgress");
		IMS_BLOCK_ERROR_INTERNAL_MAP.put("ETIMECH39","City time is changed");
	}
	
	/** IMS error id to  IMS error code map */
	public static Map<String, Integer> IMS_BLOCK_STATUS_CODE_MAP = new HashMap<>();
	static {
		IMS_BLOCK_STATUS_CODE_MAP.put("EIMSFAIL39",417); 
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATRES39",417); 
		IMS_BLOCK_STATUS_CODE_MAP.put("EFAREMISMATCH39",417); 
		IMS_BLOCK_STATUS_CODE_MAP.put("EPROBLOCK39",417); 
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATFDS39",417); 
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATDS39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATMF39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATF39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ELBAL39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATFM39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EUSER39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EDEPT39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EPARAM39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EFAREMISMATCH39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESERVC39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EAGECHK39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EAGECHK39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ECHLDTRVL39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ETIMEOUT39",408);
		IMS_BLOCK_STATUS_CODE_MAP.put("EPARAM39",400);
		IMS_BLOCK_STATUS_CODE_MAP.put("EAUTH39",401);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATFM39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ETWOSINGLE39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EGENDER39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EMISSID39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EWINDOW39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATCH39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATX39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ELASTF39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATDB39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ESEATMAX39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EPAXZERO39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EBPINVALID39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("EROUTX39",417);
		IMS_BLOCK_STATUS_CODE_MAP.put("ETIMECH39",417);
	}
	
	
	/** bitla error code to IMS error id map */
	public static Map<String, String> IMS_BITLA_BLOCK_ERROR_MAP = new HashMap<>();
	static {
		IMS_BITLA_BLOCK_ERROR_MAP.put("403","ESEATRES39"); 
		IMS_BITLA_BLOCK_ERROR_MAP.put("500","ESEATRES39"); 
		IMS_BITLA_BLOCK_ERROR_MAP.put("406","EPROBLOCK39"); 
		IMS_BITLA_BLOCK_ERROR_MAP.put("410","EPROBLOCK39"); 
		IMS_BITLA_BLOCK_ERROR_MAP.put("420","ESEATMF39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("412","EPARAM39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("413","EPARAM39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("422","EPARAM39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("444","EFAREMISMATCH39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("402","ESERVC39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("400","EPARAM39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("401","EAUTH39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("404","ESEATMAX39");
		IMS_BITLA_BLOCK_ERROR_MAP.put("434","EROUTX39");
	}
	
	
	/** IMS Seat layout api error details */
	
	public static Map<String, String> IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP = new HashMap<>();
	static {
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EDEPTD39", "trip_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("ESOLD39", "soldout");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EMISSTRIP39", "trip_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EFAREMISMATCH39", "trip_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("ESERVU39", "trip_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EROUTU39", "trip_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EAUTH39", "trip_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("ETIMEOUT39", "timeout");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EPARAM39", "bad_request");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EUSER39", "unauthorised");
		IMS_SEAT_LAYOUT_ERROR_CATEGORY_MAP.put("EIMSFAIL39", "provider_service_failed");
	}
	
	public static Map<String, String> IMS_SEAT_LAYOUT_ERROR_CODE_MAP = new HashMap<>();
	static {
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EDEPTD39", "bus_departed");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("ESOLD39", "soldout");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EMISSTRIP39", "trip_not_found");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EFAREMISMATCH39", "service_cancelled");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("ESERVU39", "service_no_longer_running");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EROUTU39", "route_unavailable");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EAUTH39", "authentication_failed");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("ETIMEOUT39", "request_timeout");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EPARAM39", "invalid_parameters");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EUSER39", "invalid_user");
		IMS_SEAT_LAYOUT_ERROR_CODE_MAP.put("EIMSFAIL39", "trip_failed");
	}
	
	public static Map<String, String> IMS_SEAT_LAYOUT_INTERNAL_MAP = new HashMap<>();
	static {
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EDEPTD39", "Bus departed");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("ESOLD39", "Soldout");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EMISSTRIP39", "No Trip Found");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EFAREMISMATCH39", "Service Cancelled");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("ESERVU39", "Service no longer running!");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EROUTU39", "Route Unavailable");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EAUTH39", "Error in getting authentication");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("ETIMEOUT39", "Timeout");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EPARAM39", "Bad Request");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EUSER39", "Authentication");
		IMS_SEAT_LAYOUT_INTERNAL_MAP.put("EIMSFAIL39", "Others");
	}
	
	/** IMS error id to  IMS error code map */
	public static Map<String, Integer> IMS_SEAT_LAYOUT_STATUS_CODE_MAP = new HashMap<>();
	static {
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EDEPTD39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("ESOLD39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EMISSTRIP39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EFAREMISMATCH39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("ESERVU39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EROUTU39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EAUTH39", 417);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("ETIMEOUT39", 408);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EPARAM39", 400);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EUSER39", 401);
		IMS_SEAT_LAYOUT_STATUS_CODE_MAP.put("EIMSFAIL39", 417);
	}
	
	/** map Bitla error code to IMS error id */
	public static Map<String, String> IMS_BITLA_SEAT_LAYOUT_ERROR_MAP = new HashMap<>();
	static {
		IMS_BITLA_SEAT_LAYOUT_ERROR_MAP.put("416","EDEPTD39");
		IMS_BITLA_SEAT_LAYOUT_ERROR_MAP.put("402","ESERVU39");
		IMS_BITLA_SEAT_LAYOUT_ERROR_MAP.put("400","EPARAM39");
		IMS_BITLA_SEAT_LAYOUT_ERROR_MAP.put("412","EPARAM39");
		IMS_BITLA_SEAT_LAYOUT_ERROR_MAP.put("401","EUSER39");
	}
}

