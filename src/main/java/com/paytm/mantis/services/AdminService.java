package com.paytm.mantis.services;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.utilities.AerospikeManager;
import com.paytm.mantis.utilities.RestClientUtil;
import com.paytm.mantis.utilities.Utility;
import com.paytm.mantis.v2.api.Operator;
import com.paytm.mantis.v2.api.OperatorsList;

@Path("/admin")
public class AdminService {
	Logger LOGGER = LoggerFactory.getLogger("mantis_logging");
	
	private RestClientUtil restClientUtil;

	public AdminService(RestClientUtil clientUtil){
		this.restClientUtil = clientUtil;
	}
	
	
	
	@Path("/reload_cached_operators")
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public String reloadCachedOperators(){
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		return aerospikeManager.reloadCachedOperators();
	}
	
	@Path("/list_operators")
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public OperatorsList getAllOperators(){
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		List<Operator> allOperatorsList = aerospikeManager.getAllOperators();
		OperatorsList operatorsResponse = new OperatorsList();
		
		operatorsResponse.setAllOpeators(allOperatorsList);
		if(allOperatorsList!=null && allOperatorsList.size() > 0) {
			operatorsResponse.setStatusCode(Constants.STATUS_CODE_200);
		}else {
			operatorsResponse = (OperatorsList) Utility.setFailure(operatorsResponse);
		}
		return operatorsResponse;
	}
	
	@Path("/list_enabled_operators")
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public OperatorsList getAllEnabledOperators(){
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		List<Operator> allOperatorsList = aerospikeManager.getAllEnabledOperators();
		OperatorsList operatorsResponse = new OperatorsList();
		
		operatorsResponse.setAllOpeators(allOperatorsList);
		operatorsResponse.setStatusCode(Constants.STATUS_CODE_200);
		return operatorsResponse;
	}
	
	/**
	 * update operator - enable or disable operator
	 * @param operation
	 * @param operatorId
	 * @return
	 */
	@Path("/update_operator")
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public Operator updateOperators(@QueryParam("operation") String operation,@QueryParam("operatorId") String operatorId){
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		Operator operator = new Operator();
		operator.setOperatorId(operatorId);
		try {
			if(operation.isEmpty() && !operation.equalsIgnoreCase("enable") && !operation.equalsIgnoreCase("disable")) {
				return operator;
			}
	
			if(operation.equalsIgnoreCase("enable")) {
				operator.setEnabled(true);
			}
			else if(operation.equalsIgnoreCase("disable")) {
				operator.setEnabled(false);
			}
		}catch(Exception exception) {
			LOGGER.error("Exception occurred in updateOperators() "+exception.getMessage());
		}
		return aerospikeManager.enableDisableOperator(operator);
	}
	
	/**
	 * init operators
	 * @return
	 */
	@Path("/init_operators")
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public String intilizeOperators() {
		String response = "Operators Initilized Successfully";
		String failedOperatorIds = "";
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		OperatorsService operatorsService= new OperatorsService(restClientUtil);
		
		try {
			OperatorsList operatorsList = operatorsService.getOperators();
			
			if(operatorsList!=null && operatorsList.getStatusCode()==Constants.STATUS_CODE_200) {
				for (Operator operator : operatorsList.getAllOpeators()) {
					Operator insertedOperator = aerospikeManager.addOperator(operator);
					if(insertedOperator==null || (insertedOperator!=null && Utility.isEmpty(insertedOperator.getOperatorId()))) {
						failedOperatorIds += operator.getOperatorId()+",";
					}
				}
			}else {
				response = "Error occurred while initializing Operators. Check Bitla operators api response";
			}
		}catch(Exception exception) {
			LOGGER.error("Exception occurred in intilizeOperators() "+exception.getMessage());
			response = "Exception occurred while initializing Operators";
		}
		if(!Utility.isEmpty(failedOperatorIds) && failedOperatorIds.length()>1) {
			response = "Insertion in Aerospike failed for opeartor ids  = "+failedOperatorIds;
		}
		return response;
	}
	
	@Path("/reload_operators")
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public String reloadOperators() {
		String response = "Operators Reloaded Successfully";
		OperatorsService operatorsService= new OperatorsService(restClientUtil);
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		
		try {
			OperatorsList operatorsList = operatorsService.getOperators();
			if(operatorsList!=null && operatorsList.getStatusCode()==Constants.STATUS_CODE_200) {
				for (Operator operator : operatorsList.getAllOpeators()) {
					aerospikeManager.updateOperator(operator);
				}
			}else {
				response = "Error occurred while reloading Operators. Check Bitla operators api response";
			}
		}catch(Exception exception) {
			LOGGER.error("Exception occurred in reloadOperators() "+exception.getMessage());
			response = "Exception occurred while reloading Operators";
		}
		/** reload cached operators list */
		reloadCachedOperators();
		
		return response;
	}
	
}
