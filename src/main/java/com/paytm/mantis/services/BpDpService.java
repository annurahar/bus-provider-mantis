package com.paytm.mantis.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpMethod;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.utilities.AerospikeManager;
import com.paytm.mantis.utilities.RestClientUtil;
import com.paytm.mantis.utilities.Utility;
import com.paytm.mantis.v2.api.SearchV2Response;
import com.paytm.mantis.v2.api.StageDetail;

@Path("/bpdp_points")
@Produces(MediaType.APPLICATION_JSON)
public class BpDpService {
	Logger LOGGER = LoggerFactory.getLogger("mantis_logging");
	private RestClientUtil restClientUtil;

	public BpDpService(RestClientUtil clientUtil) {
		this.restClientUtil = clientUtil;
	}
	@GET
	@Timed
	public void search() {
		String sourceId = "1";
		String destinationId = "11";
		String date = "2019-03-10";
		StringBuilder requestUrl = new StringBuilder();
		SearchV2Response searchV2Response = new SearchV2Response();
		requestUrl.append("APISearchRoutesV2");
		LinkedHashMap<String, String> params = new LinkedHashMap<>();
		params.put(Constants.FROMCITYID, sourceId);
		params.put(Constants.TOCITYID, destinationId);
		params.put(Constants.JOURNEYDATE, Utility.convertDate(date));
		String response = restClientUtil.getResponse(requestUrl.toString(), null, HttpMethod.GET, params);
		try {
			if (!Utility.isEmpty(response)) {
				JSONObject data = XML.toJSONObject(response);
				JSONObject resultJsonObject = data.getJSONObject(Constants.APISEARCHRESPONSE)
						.getJSONObject(Constants.APISEARCHRESULT);
				if (resultJsonObject.get(Constants.ROUTE) != null) {
					Object searchObject = resultJsonObject.get(Constants.ROUTE);
					JSONArray searchJsonArray = null;
					if (searchObject instanceof JSONArray) {
						searchJsonArray = resultJsonObject.getJSONArray(Constants.ROUTE);
					} else if (searchObject instanceof JSONObject) {
						String json1 = resultJsonObject.getJSONObject(Constants.PICKUP).toString();
						searchJsonArray = new JSONArray("[" + json1 + "]");				         
					}
					BPDPMatis(searchJsonArray);
				}
			} else {
				Utility.setFailure(searchV2Response);
			}
		} catch (Exception exception) {
			Utility.setFailure(searchV2Response);
		}
	}

	private void BPDPMatis(JSONArray searchJsonArray) {
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		for (int i = 0; i < searchJsonArray.length(); i++) {
			JSONObject service = searchJsonArray.getJSONObject(i);
			JSONObject bpdpResponseObject = getBPDPpoints(Utility.getPropertyValue(service, Constants.ROUTECODE));
			String[] route_codes = Utility.getPropertyValue(service, Constants.ROUTECODE).split("~");
			String route_ids = route_codes[0] + "|" + route_codes[1] + "|" + route_codes[2] + "|" + route_codes[3];
			if (!getBoardingPoints(bpdpResponseObject).isEmpty()) {
				aerospikeManager.addBpDpPoints(getBoardingPoints(bpdpResponseObject), route_ids+"|"+"BP");
			}
			if (!getDroppingPoints(bpdpResponseObject).isEmpty()) {
				aerospikeManager.addBpDpPoints(getDroppingPoints(bpdpResponseObject), route_ids+"|"+"DP");
			}
		}
	}
	
	/**
	 * returns BPDP details
	 * @param bus routeCode
	 * @return
	 */
	private JSONObject getBPDPpoints(String propertyValue) {
		LinkedHashMap<String, String> params = new LinkedHashMap<>();
		JSONObject resultJsonObject = null;
		StringBuilder requestUrl = new StringBuilder();
		requestUrl.append("APIGetPickupsDropoffs");
		params.put(Constants.ROUTEID, propertyValue);
		String response = restClientUtil.getResponse(requestUrl.toString(), null, HttpMethod.GET, params);
		try {
			if (!Utility.isEmpty(response)) {
				JSONObject data = XML.toJSONObject(response);
				resultJsonObject = data.getJSONObject(Constants.APIPICKUPDROPOFF)
						.getJSONObject(Constants.APIPICKUPDROPOFFRESULT);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred in getBPDPpoints " + exception.getMessage());
		}
		return resultJsonObject;
	}

	/**
	 * returns boarding points
	 * @param bpdp JSONObject
	 * @return
	 */
	private List<StageDetail> getBoardingPoints(JSONObject jsonbpdp) {
		JSONArray boardingjson = null;
		Object boardingResponse = null;
		ArrayList<StageDetail> stageList = new ArrayList<>();
		JSONObject boardingDetalis = jsonbpdp.getJSONObject(Constants.PICKUPS);
		if (boardingDetalis != null && !boardingDetalis.isEmpty()) {
			
			/** Check type of response and convert it in JSONArray*/
			boardingResponse = boardingDetalis.getJSONArray(Constants.PICKUP);
			if (boardingResponse instanceof JSONObject) {
				String json1 = boardingDetalis.getJSONObject(Constants.PICKUP).toString();
				boardingjson = new JSONArray("[" + json1 + "]");
			} else if (boardingResponse instanceof JSONArray) {
				boardingjson = boardingDetalis.getJSONArray(Constants.PICKUP);
			}
			for (int i = 0; i < boardingjson.length(); i++) {
				boardingDetalis = boardingjson.getJSONObject(i);
				StageDetail stages = new StageDetail();
				stages.setId(Utility.getPropertyValue(boardingDetalis, Constants.PICKUPID));
				stages.setAddress(Utility.getPropertyValue(boardingDetalis, Constants.Address));
				stages.setName(Utility.getPropertyValue(boardingDetalis, Constants.PICKUPNAME));
				stages.setDateTime(getDateTime(Utility.getPropertyValue(boardingDetalis, Constants.PICKUPTIME)));
				stages.setLandmark(Utility.getPropertyValue(boardingDetalis, Constants.PICKUPLANDMARK));
				if (!Utility.isEmpty(Utility.getPropertyValue(boardingDetalis, Constants.PICKUPCONTACTS))) {
					String[] contacts = Utility.getPropertyValue(boardingDetalis, Constants.PICKUPCONTACTS).trim()
							.split(",");
					stages.setContactNumber(Arrays.asList(contacts));
				}
				stageList.add(stages);
			}
			stageList.sort((StageDetail sd1, StageDetail sd2) -> {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime localDateTime1 = LocalDateTime.parse(sd1.getDateTime(), formatter);
				LocalDateTime localDateTime2 = LocalDateTime.parse(sd2.getDateTime(), formatter);

				return localDateTime1.compareTo(localDateTime2);
			});
		}
		return stageList;
	}
	
	/**
	 * returns dropping points
	 * @param bpdp object
	 * @return
	 */
	private List<StageDetail> getDroppingPoints(JSONObject jsonbpdp) {
		Object droppingResponse = null;
		JSONArray droppingJsonArray = null;
		ArrayList<StageDetail> stageList = new ArrayList<>();
		JSONObject droppingDetalis = jsonbpdp.getJSONObject(Constants.DROPOFFS);
		if (droppingDetalis != null && !droppingDetalis.isEmpty()) {
			droppingResponse = droppingDetalis.get(Constants.DROPOFF);
			
			/** Check type of response and convert it in JSONArray*/
			if (droppingResponse instanceof JSONObject) {
				String json1 = droppingDetalis.getJSONObject(Constants.DROPOFF).toString();
				droppingJsonArray = new JSONArray("[" + json1 + "]");
			} else if (droppingResponse instanceof JSONArray) {
				droppingJsonArray = droppingDetalis.getJSONArray(Constants.DROPOFF);
			}
			for (int i = 0; i < droppingJsonArray.length(); i++) {
				droppingDetalis = droppingJsonArray.getJSONObject(i);
				StageDetail stages = new StageDetail();
				stages.setId(Utility.getPropertyValue(droppingDetalis, Constants.DROPUPID));
				stages.setAddress(Utility.getPropertyValue(droppingDetalis, Constants.DROPNAME));
				stages.setName(Utility.getPropertyValue(droppingDetalis, Constants.DROPNAME));
				stages.setDateTime(getDateTime(Utility.getPropertyValue(droppingDetalis, Constants.DROPUPTIME)));
				stageList.add(stages);
			}
			stageList.sort((StageDetail sd1, StageDetail sd2) -> {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime localDateTime1 = LocalDateTime.parse(sd1.getDateTime(), formatter);
				LocalDateTime localDateTime2 = LocalDateTime.parse(sd2.getDateTime(), formatter);

				return localDateTime1.compareTo(localDateTime2);
			});
		}
		return stageList;
	}
	/**
	 * returns arriaveDateTime or departuredateTime
	 * @param dateTime
	 * @return
	 */

	private String getDateTime(String propertyValue) {
		String[] dateTime = propertyValue.split(" ");
		String depTime = Utility.convertTo24HoursFormat(dateTime[1]+" "+dateTime[2]);
		return dateTime[0]+" "+depTime;
	}

}
