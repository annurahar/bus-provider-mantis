package com.paytm.mantis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpMethod;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.utilities.RestClientUtil;
import com.paytm.mantis.utilities.Utility;
import com.paytm.mantis.v2.api.Operator;
import com.paytm.mantis.v2.api.OperatorsList;

@Produces(MediaType.APPLICATION_JSON)
public class OperatorsService {
	Logger LOGGER = LoggerFactory.getLogger("mantis_logging");
	
	private RestClientUtil restClientUtil;

	public OperatorsService(RestClientUtil clientUtil){
		this.restClientUtil = clientUtil;
	}
	
	@GET
	@Timed
	public OperatorsList getOperators() {
		/** build Operators request url */
		OperatorsList operatorsResponse = null;
		
		/** build soperator service request url */
		StringBuilder requestUrl = new StringBuilder();
		requestUrl.append("APIGetCompanies");

		String response = restClientUtil.getResponse(requestUrl.toString(), null, HttpMethod.GET,null);
		
		/** Transforming XML response to JSON response*/
		try {
			if (!Utility.isEmpty(response)) {
				JSONObject data = XML.toJSONObject(response);
				JSONObject resultJsonObject = null;
				if(!data.getJSONObject(Constants.APIGETCOMPANIESRESPONSE).isEmpty())
					resultJsonObject = data.getJSONObject(Constants.APIGETCOMPANIESRESPONSE).getJSONObject(Constants.APIGETCOMPANIESRESULT);
						
				if (resultJsonObject!=null && !resultJsonObject.isEmpty() && resultJsonObject.getJSONArray(Constants.COMPANY) != null
						&& !resultJsonObject.getJSONArray(Constants.COMPANY).isEmpty()) {
					JSONArray companyJsonArray = resultJsonObject.getJSONArray(Constants.COMPANY);
					operatorsResponse = getTransformedResponse(requestUrl.toString(), companyJsonArray);
				}
			}else
				Utility.setFailure(operatorsResponse);
		} catch (Exception exception) {
			Utility.setFailure(operatorsResponse);
			LOGGER.error("Exception occurred in OperatorsService "+exception);
		}
		
		/** return transformed result */
		return operatorsResponse;
	}

	/**
	 * returns transformed result
	 * @param request
	 * @param companyJsonArray
	 * @return
	 */
	private OperatorsList getTransformedResponse(String request, JSONArray companyJsonArray) {
		OperatorsList  operatorsResponse = new OperatorsList();
		List<Operator> operatorsList = new ArrayList<>();
		JSONObject companyObject = null; 
		
		try {
			for (int index = 0; index < companyJsonArray.length(); index++) {
				companyObject = companyJsonArray.getJSONObject(index);
				Operator operator = new Operator();
				operator.setOperatorId(Utility.getPropertyValue(companyObject, Constants.COMPANYID));
				operator.setOperatorName(Utility.getPropertyValue(companyObject, Constants.COMPANYNAME));
				operatorsList.add(operator);
			}
			operatorsResponse.setAllOpeators(operatorsList);
			if(operatorsList!=null && !operatorsList.isEmpty())
				operatorsResponse.setStatusCode(Constants.STATUS_CODE_200);
			else
				Utility.setFailure(operatorsResponse);
		}catch(Exception exception) {
			Utility.setFailure(operatorsResponse);
			LOGGER.error("Exception occurred in getTransformedResponse() of OperatorsService "+exception);
		}
		return operatorsResponse;
	}
}
