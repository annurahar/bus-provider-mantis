package com.paytm.mantis.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpMethod;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.utilities.AerospikeManager;
import com.paytm.mantis.utilities.RestClientUtil;
import com.paytm.mantis.utilities.Utility;
import com.paytm.mantis.v2.api.CancellationPolicy;
import com.paytm.mantis.v2.api.Operator;

@Path("/cancellation_policy")
@Produces(MediaType.APPLICATION_JSON)
public class CancellationPolicyService {
Logger LOGGER = LoggerFactory.getLogger("mantis_logging");
	
	private RestClientUtil restClientUtil;

	public CancellationPolicyService(RestClientUtil clientUtil){
		this.restClientUtil = clientUtil;
	}
	
	@GET
	@Timed
	public String getCancellationPolicy() {
		String response = "Failed to load cancellation policy.";
		ArrayList<CancellationPolicy> cancellationPolicyResponseList = new ArrayList<CancellationPolicy>();
		
		try {
			AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
			Iterator<Operator> iterator = AerospikeManager.allOperatorsList.iterator();
			
			while(iterator.hasNext()) {
				String operatorId = iterator.next().getOperatorId();
			
				/** build CancellationPolicy request url */
				StringBuilder requestUrl = new StringBuilder();
				requestUrl.append("APIGetCancellationPolicies");
		
				/** set query params */
				HashMap<String,String> queryParams = new HashMap<String,String>();
				queryParams.put(Constants.COMPANY_ID, operatorId);
				
				String travelyaariResponse = restClientUtil.getResponse(requestUrl.toString(), null, HttpMethod.GET,queryParams);
			
				/** Transforming XML response to JSON response*/
				if (!Utility.isEmpty(travelyaariResponse)) {
					JSONObject data = XML.toJSONObject(travelyaariResponse);
					JSONObject resultJsonObject = null;
					if(!data.getJSONObject(Constants.APIGETCANCELLATIONPOLICIESRESPONSE).isEmpty())
						resultJsonObject = data.getJSONObject(Constants.APIGETCANCELLATIONPOLICIESRESPONSE).getJSONObject(Constants.APIGETCANCELLATIONPOLICIESRESULT);
							
					if (resultJsonObject!=null && !resultJsonObject.isEmpty() && resultJsonObject.getJSONArray(Constants.CANCELLATIONPOLICY) != null
							&& !resultJsonObject.getJSONArray(Constants.CANCELLATIONPOLICY).isEmpty()) {
						JSONArray policyJsonArray = resultJsonObject.getJSONArray(Constants.CANCELLATIONPOLICY);
						cancellationPolicyResponseList = getTransformedResponse(requestUrl.toString(),operatorId, policyJsonArray);
					}
					
					/** add cancellation policy in aerospike */
					if(Utility.isNotEmptyList(cancellationPolicyResponseList)) {
						response = aerospikeManager.addPolicy(cancellationPolicyResponseList);
					}
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred in cancellationPolicyService ",exception);
		}
		return response;
	}

	/**
	 * returns policy in format -> "0-3|No Cancellation,3-24|100,24-72|30,72-168|20,168-720|10"
	 * @param operatorId 
	 * @param string
	 * @param policyJsonArray
	 * @return
	 */
	private ArrayList<CancellationPolicy> getTransformedResponse(String request, String operatorId, JSONArray policyJsonArray) {
		ArrayList<CancellationPolicy> policyList = new ArrayList<CancellationPolicy>();
		CancellationPolicy cancellationPolicy = new CancellationPolicy();
		JSONObject policyObject = null; 
		ArrayList<Integer> hoursList = new ArrayList<Integer>();
		StringBuilder policy = new StringBuilder();
		ArrayList<String> dateList = new ArrayList<String>();
		int uniqueDateCounter = -1;
		
		try {
			for (int index = 0; index < policyJsonArray.length(); index++) {
				
				policyObject = policyJsonArray.getJSONObject(index);
				int chargeAmount = Integer.parseInt(Utility.getPropertyValue(policyObject, Constants.CHARGEAMT));
				int chargePCT = Integer.parseInt(Utility.getPropertyValue(policyObject, Constants.CHARGEPCT));
				String journeyDate = new String (Utility.getPropertyValue(policyObject, Constants.CHARTDATE));
				int MinsBefore = Integer.parseInt(Utility.getPropertyValue(policyObject, Constants.MINSBEFORE));
				
				/** reset date counter and add ready policy in list for a date*/
				if(!dateList.contains(journeyDate)) {
					cancellationPolicy = new CancellationPolicy();
					if(index!=0 && Utility.isNotEmpty(journeyDate) && Utility.isNotEmpty(policy.toString())) {
						if(policy.charAt(policy.length()-1)==',')
							cancellationPolicy.setCancellationPolicy(policy.substring(0, policy.length()-1));
						else
							cancellationPolicy.setCancellationPolicy(policy.toString());
						cancellationPolicy.setOperatorId(operatorId);
						cancellationPolicy.setJourneyDate(journeyDate);
						cancellationPolicy.setStatusCode(Constants.STATUS_CODE_200);
					}else
						Utility.setFailure(cancellationPolicy);
					policyList.add(cancellationPolicy);
					
					dateList.add(journeyDate);
					uniqueDateCounter = 0;
					policy = new StringBuilder();
				}
				
				/**convert  minutes to hours */
				int hours = MinsBefore / 60;
				hoursList.add(hours);
				
				if(uniqueDateCounter==0) 
					policy.append("0-"+hours).append("|");
				else 
					policy.append(hoursList.get(index-1)+"-"+hours).append("|");
				
				if(chargeAmount==0)
					policy.append(chargePCT);
				else
					policy.append(chargeAmount+Constants.INR);
				policy.append(",");
				
				uniqueDateCounter++;
			}
			
		}catch(Exception exception) {
			Utility.setFailure(cancellationPolicy);
			LOGGER.error("Exception occurred in getTransformedResponse() of CancellationPolicyService ",exception);
		}
		
		return policyList;
	}
}
