package com.paytm.mantis.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpMethod;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.utilities.RestClientUtil;
import com.paytm.mantis.utilities.Utility;
import com.paytm.mantis.v2.api.FareBreakup;
import com.paytm.mantis.v2.api.SeatLayoutV2Request;
import com.paytm.mantis.v2.api.SeatLayoutV2Response;
import com.paytm.mantis.v2.api.SeatLayoutV2Response.Seats;
import com.paytm.mantis.v2.api.SeatLayoutV2Response.Type;

@Path("/api/seatLayout")
@Produces(MediaType.APPLICATION_JSON)
public class SeatLayoutService {
	Logger LOGGER = LoggerFactory.getLogger("travelyaari_logging");
	
	private RestClientUtil restClientUtil;
	List<String> gentsSeats =  null;
	List<String> ladiesSeats = null;
	Set<Double> uniqueFare = null;
	

	public SeatLayoutService(RestClientUtil clientUtil){
		this.restClientUtil = clientUtil;
	}

	@POST
	@Timed
	public SeatLayoutV2Response getSeatLayout(SeatLayoutV2Request request,@HeaderParam("request_id") String requestId) {
		/** IMS request start time */
		final long IMSRequestStartTime = System.nanoTime();
		SeatLayoutV2Response seatLayoutV2Response = new SeatLayoutV2Response();
		
		/** build seat layout request url */
		StringBuilder requestUrl = new StringBuilder();
		requestUrl.append("APIGetChart");
		
		/** set query params */
		HashMap<String,String> queryParams = new HashMap<String,String>();
		queryParams.put(Constants.ROUTE_CODE, request.getTripId());
		
		/** Mantis request start time */
		final long travelyaariRequestStartTime = System.nanoTime();
		String response = restClientUtil.getResponse(requestUrl.toString(), null, HttpMethod.GET,queryParams);
		/** Mantis request end time */
		final long travelyaariRequestEndTime = System.nanoTime(); 
		
		/** Transforming XML response to JSON response*/
		try {
			if (!Utility.isEmpty(response)) {
				JSONObject data = XML.toJSONObject(response);
				JSONObject resultJsonObject = null;
				if(data!=null && !data.isEmpty() && !data.getJSONObject(Constants.APIGetChartResponse).isEmpty())
					resultJsonObject = data.getJSONObject(Constants.APIGetChartResponse).getJSONObject(Constants.APIGetChartResult);
						
				
				if (resultJsonObject!=null && !resultJsonObject.isEmpty() && !resultJsonObject.isEmpty() 
						&& resultJsonObject.getJSONArray(Constants.Seat) != null
						&& !resultJsonObject.getJSONArray(Constants.Seat).isEmpty()) {
					JSONArray seatJsonArray = resultJsonObject.getJSONArray(Constants.Seat);
					seatLayoutV2Response = getTransformedResponse(requestUrl.toString(), seatJsonArray);
				}
			}else
				Utility.setFailure(seatLayoutV2Response);
		} catch (Exception exception) {
			Utility.setFailure(seatLayoutV2Response);
			LOGGER.error("Exception occurred in SeatLayoutService "+exception);
		}
		
		/** IMS request end time */
		final long IMSRequestEndTime = System.nanoTime();
		
		/** Logging travelyaari request start/end time details for monitoring purpose */
		long duration =  TimeUnit.NANOSECONDS.toMillis(travelyaariRequestEndTime-travelyaariRequestStartTime);
		long startTime =  TimeUnit.NANOSECONDS.toMillis(travelyaariRequestStartTime);
		long endTime =  TimeUnit.NANOSECONDS.toMillis(travelyaariRequestEndTime);
		String operatorId = Utility.isEmpty(request.getOperatorId()) ? "-1" : request.getOperatorId(); 
		
		/** publish travelyaari logs */
		Utility.publishMonitoringLogs(Constants.TRAVELYAARI,Constants.SEAT_LAYOUT,requestId,operatorId,duration,startTime,endTime,seatLayoutV2Response.getStatusCode(),seatLayoutV2Response.getErrorid());
		
		/** Logging IMS request start/end time details for monitoring purpose */
		duration =  TimeUnit.NANOSECONDS.toMillis(IMSRequestEndTime-IMSRequestStartTime);
		startTime =  TimeUnit.NANOSECONDS.toMillis(IMSRequestStartTime);
		endTime =  TimeUnit.NANOSECONDS.toMillis(IMSRequestEndTime);
		
		/** publish IMS logs */
		Utility.publishMonitoringLogs(Constants.IMS,Constants.SEAT_LAYOUT,requestId,operatorId,duration,startTime,endTime,seatLayoutV2Response.getStatusCode(),seatLayoutV2Response.getErrorid());
		
		return seatLayoutV2Response;
	}

	/**
	 * returns transformed response
	 * @param string
	 * @param companyJsonArray
	 * @return
	 * 
	 * <a:Seat>
            <a:ColumnNo>0</a:ColumnNo>
            <a:CompanyChartID>6322</a:CompanyChartID>
            <a:CompanyChartName>2X1(40) AC Seater-Sleeper new-2</a:CompanyChartName>
            <a:Deck>2</a:Deck>
            <a:Fare>578</a:Fare>
            <a:Gender/>
            <a:HasSCharge>1</a:HasSCharge>
            <a:HasSTax>1</a:HasSTax>
            <a:Height>2</a:Height>
            <a:IsAC>true</a:IsAC>
            <a:IsAisle>false</a:IsAisle>
            <a:IsAvailable>1</a:IsAvailable>
            <a:RouteCode>48~87~33~1~2019-03-12</a:RouteCode>
            <a:RowNo>0</a:RowNo>
            <a:SeatLabel>C</a:SeatLabel>
            <a:ServiceCharge>0</a:ServiceCharge>
            <a:ServiceTax>28</a:ServiceTax>
            <a:Width>1</a:Width>
        </a:Seat>
        
        
	 */
	private SeatLayoutV2Response getTransformedResponse(String request, JSONArray seatJsonArray) {
		SeatLayoutV2Response seatLayoutV2Response = new SeatLayoutV2Response();
		List<Seats> seatsList = new ArrayList<Seats>(); 
		List<FareBreakup> uniqueFareBreakUps = new ArrayList<FareBreakup>(); 
		
		if(Utility.isNotEmptyJsonArray(seatJsonArray)) {
			try {
				for (int index = 0; index < seatJsonArray.length(); index++) {
					JSONObject seatJson = seatJsonArray.getJSONObject(index);
					SeatLayoutV2Response.Seats seat = new Seats();
					
					/** column */
					seat.setColumn(Integer.parseInt(Utility.getPropertyValue(seatJson, Constants.ColumnNo)));
					
					/** fare */
					Double fare = Double.parseDouble(Utility.getPropertyValue(seatJson, Constants.Fare));
					//Question to travelyaari
					/*if(Utility.getPropertyValue(seatJson, Constants.HasSCharge).trim().equals("1"))
						fare = fare + Double.parseDouble(Utility.getPropertyValue(seatJson, Constants.ServiceCharge));*/
					
					// fare returned by travelyaari is inclusive of service tax as per the documentation
					seat.setFare(fare);
					seat.setTaxInclusive(true);
					
					/** 1= available, 0=unavailable*/
					if(seat.getFare()== new Double(0))
						seat.setAvailable(false);
					else
						seat.setAvailable(Utility.getPropertyValue(seatJson, Constants.IsAvailable).trim().equals("1"));
					
					/** fare break up */
					FareBreakup fareBreakup = new FareBreakup();
					fareBreakup.setTotalFare(fare);
					if(Utility.getPropertyValue(seatJson, Constants.HasSTax).trim().equals("1"))
						fareBreakup.setServiceTax(Double.parseDouble(Utility.getPropertyValue(seatJson, Constants.ServiceTax)));
					fareBreakup.setBaseFare(fareBreakup.getTotalFare() - fareBreakup.getServiceTax());
					
					if(!uniqueFareBreakUps.contains(fareBreakup))
						uniqueFareBreakUps.add(fareBreakup);
					
					/** FareBreakupType */
					seat.setFareBreakupType(uniqueFareBreakUps.indexOf(fareBreakup));
					
					/** setLength Width :: 1=Seater, 2=Sleeper 
					 * Height :: 1=Seater, 2=Sleeper  * */
					seat.setLength(Integer.parseInt(Utility.getPropertyValue(seatJson, Constants.Height)));
					
					/** setLowerBerth, Deck ::  1=Lower Berth, 2 = Upper Birth*/
					seat.setLowerBerth(Utility.getPropertyValue(seatJson, Constants.Deck).trim().equals("1"));
					
					/** female seat */
					seat.setReservedForLadies(Utility.getPropertyValue(seatJson, Constants.Gender).trim().equalsIgnoreCase("F"));
					
					/** male seat */
					seat.setReservedForMen(Utility.getPropertyValue(seatJson, Constants.Gender).trim().equalsIgnoreCase("M"));
					
					/** setRow*/
					seat.setRow(Integer.parseInt(Utility.getPropertyValue(seatJson, Constants.RowNo)));
					
					/** setSeatNumber */
					seat.setSeatNumber(Utility.getPropertyValue(seatJson, Constants.SeatLabel));
					
					/** seat Type */
					if(seat.getLength()==1) // 1= seater
						seat.setType((Type.S).name());
					else
						seat.setType((Type.SL).name()); // 2= sleeper
					
					/** setWidth */
					seat.setWidth(Integer.parseInt(Utility.getPropertyValue(seatJson, Constants.Width)));
					
					/** add seat  to list */
					seatsList.add(seat);
				}
				
				/** Set Seat layout response elements */
				seatLayoutV2Response.setCollectPaxid(false);
				seatLayoutV2Response.setFareBreakupList(uniqueFareBreakUps);
				seatLayoutV2Response.setSeats(seatsList);
				
				if(Utility.isEmptyList(uniqueFareBreakUps) && Utility.isEmptyList(seatsList))
					Utility.setFailure(seatLayoutV2Response);
				else	
					seatLayoutV2Response.setStatusCode(Constants.STATUS_CODE_200);
				
			}catch(Exception exception) {
				Utility.setFailure(seatLayoutV2Response);
				LOGGER.error("Exception occurred :: ",exception);
			}
			
		}else
			Utility.setFailure(seatLayoutV2Response);
		/** Log request and response if not success status code */
		if(seatLayoutV2Response.getStatusCode()!=Constants.STATUS_CODE_200) {
			Gson gson = new Gson();
			LOGGER.error("Erroneous Response from Bitla :: "+gson.toJson(seatLayoutV2Response)+" for request :: "+request);
			LOGGER.error("Erroneous Transformed response for Erroneous bitla response :: "+gson.toJson(seatLayoutV2Response));
		}
		return seatLayoutV2Response;
	}
}
