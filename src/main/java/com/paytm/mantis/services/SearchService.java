package com.paytm.mantis.services;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpMethod;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.paytm.mantis.constants.Constants;
import com.paytm.mantis.utilities.AerospikeManager;
import com.paytm.mantis.utilities.RestClientUtil;
import com.paytm.mantis.utilities.Utility;
import com.paytm.mantis.v2.api.CancellationPolicy;
import com.paytm.mantis.v2.api.Operator;
import com.paytm.mantis.v2.api.SearchV2Request;
import com.paytm.mantis.v2.api.SearchV2Response;
import com.paytm.mantis.v2.api.SearchV2Response.Bus;
import com.paytm.mantis.v2.api.SearchV2Response.Bus.Info;
import com.paytm.mantis.v2.api.StageDetail;

@Path("/api/search")
@Produces(MediaType.APPLICATION_JSON)
public class SearchService {
Logger LOGGER = LoggerFactory.getLogger("mantis");
	
	private RestClientUtil restClientUtil;
	public SearchService(RestClientUtil clientUtil){
		this.restClientUtil = clientUtil;
	}
	
	@POST
	@Timed
	public SearchV2Response getSearchSchedules(SearchV2Request request,@HeaderParam("request_id") String requestId) {
		/** IMS request start time */
		final long IMSRequestStartTime = System.nanoTime();
		
		StringBuilder requestUrl = new StringBuilder();	
		SearchV2Response searchV2Response = new SearchV2Response();
		requestUrl.append("APISearchRoutesV2");
		LinkedHashMap<String,String> params = new LinkedHashMap<>();
		params.put(Constants.FROMCITYID, request.getSourceId());
		params.put(Constants.TOCITYID, request.getDestinationId());
		params.put(Constants.JOURNEYDATE, Utility.convertDate(request.journeyDate));
		
		/** Mantis request start time */
		final long travelyaariRequestStartTime = System.nanoTime();
		String response = restClientUtil.getResponse(requestUrl.toString(), null, HttpMethod.GET,params);
		/** Mantis request end time */
		final long travelyaariRequestEndTime = System.nanoTime();
		/** Transforming XML response to JSON response*/
		try {
			if (!Utility.isEmpty(response)) {
				JSONObject data = XML.toJSONObject(response);
				JSONObject resultJsonObject = data.getJSONObject(Constants.APISEARCHRESPONSE)
						.getJSONObject(Constants.APISEARCHRESULT);
				Object searchObject = resultJsonObject.get(Constants.ROUTE);
				if (resultJsonObject.get(Constants.ROUTE) != null) {
					JSONArray searchJsonArray = null;
					if (searchObject instanceof JSONArray) {
						searchJsonArray = resultJsonObject.getJSONArray(Constants.ROUTE);
					} else if (searchObject instanceof JSONObject) {
						String json1 = resultJsonObject.getJSONObject(Constants.PICKUP).toString();
						searchJsonArray = new JSONArray("[" + json1 + "]");
					}
					searchV2Response = transformSearchresponse(request, searchJsonArray);
				}
			}else {
				Utility.setFailure(searchV2Response);
			}
		} catch (Exception exception) {
			Utility.setFailure(searchV2Response);
			LOGGER.error("Exception occurred in getSearchSchedules "+exception.getMessage());
		}
		
		/** IMS request end time */
		final long IMSRequestEndTime = System.nanoTime();
		
		/** Logging travelyaari request start/end time details for monitoring purpose */
		long duration =  TimeUnit.NANOSECONDS.toMillis(travelyaariRequestEndTime-travelyaariRequestStartTime);
		long startTime =  TimeUnit.NANOSECONDS.toMillis(travelyaariRequestStartTime);
		long endTime =  TimeUnit.NANOSECONDS.toMillis(travelyaariRequestEndTime);
		String operatorId = "-1";
		
		/** publish travelyaari logs */
		Utility.publishMonitoringLogs(Constants.TRAVELYAARI,Constants.SEARCH,requestId,operatorId,duration,startTime,endTime,searchV2Response.getStatusCode(),searchV2Response.getErrorid());
		
		/** Logging IMS request start/end time details for monitoring purpose */
		duration =  TimeUnit.NANOSECONDS.toMillis(IMSRequestEndTime-IMSRequestStartTime);
		startTime =  TimeUnit.NANOSECONDS.toMillis(IMSRequestStartTime);
		endTime =  TimeUnit.NANOSECONDS.toMillis(IMSRequestEndTime);
		
		/** publish IMS logs */
		Utility.publishMonitoringLogs(Constants.IMS,Constants.SEARCH,requestId,operatorId,duration,startTime,endTime,searchV2Response.getStatusCode(),searchV2Response.getErrorid());
		
		return searchV2Response;
	}

	/**
	 * returns search service
	 * @param request
	 * @return
	 */
	private SearchV2Response transformSearchresponse(SearchV2Request request, JSONArray searchJsonArray) {		 
		SearchV2Response searchResponse = new SearchV2Response();
		AerospikeManager aerospikeManager = AerospikeManager.getAeroSpikeManager();
		ArrayList<Bus> busList = new ArrayList<>();
		JSONObject service;
		ArrayList<String> policyPrimaryKeyList = new ArrayList<String>();
		ArrayList<String> bpdpPrimaryKeyList = new ArrayList<>();
		HashMap<String, CancellationPolicy> cancellationPoliciesMap = new HashMap<String,CancellationPolicy>();
		HashMap<String, String> bpdpPointsmap = new HashMap<String,String>();
		try {
			/** Load cancellation policies and BP/Dp for all services before transforming search response */
			for (int index = 0; index < searchJsonArray.length(); index++) {
				service = searchJsonArray.getJSONObject(index);
				String policyPrimaryKey = Utility.getPropertyValue(service, Constants.COMPANYID).trim()+"|"+request.getJourneyDate();
				String[] route_codes = Utility.getPropertyValue(service, Constants.ROUTECODE).split("~");
				String route_ids = route_codes[0] + "|" + route_codes[1] + "|" + route_codes[2] + "|" + route_codes[3];			
				if(!policyPrimaryKeyList.contains(policyPrimaryKey))
					policyPrimaryKeyList.add(policyPrimaryKey);
				if(!bpdpPrimaryKeyList.contains(bpdpPrimaryKeyList)) {
					bpdpPrimaryKeyList.add(route_ids+"|"+"BP");
					bpdpPrimaryKeyList.add(route_ids+"|"+"DP");
				}
			}
			cancellationPoliciesMap = aerospikeManager.getCancellationPolicies(policyPrimaryKeyList);
			bpdpPointsmap = aerospikeManager.getbpdpResponse(bpdpPrimaryKeyList);
			
			/** Transforming search response */
			for (int i = 0; i < searchJsonArray.length(); i++) {
				service = searchJsonArray.getJSONObject(i);
				Bus bus = new Bus();
				bus.setSourceId(request.getSourceId());
				bus.setDestinationId(request.getDestinationId());
				bus.setBusName(Utility.getPropertyValue(service, Constants.BUSTYPE));
				bus.setBusTypeName(Utility.getPropertyValue(service, Constants.SUFFIX));
				bus.setBusClassType(Utility.getPropertyValue(service, Constants.ROUTENAME));
				bus.setDuration(getPaytmDuration(Utility.getPropertyValue(service, Constants.DEPARTURETIME), Utility.getPropertyValue(service, Constants.ARRIVALTIME)));
				bus.setAvailableSeats(Integer.parseInt(Utility.getPropertyValue(service, Constants.AVAILABILITY)));
				bus.setTripId(Utility.getPropertyValue(service, Constants.TRIPID));
				bus.setOperatorId(Utility.getPropertyValue(service, Constants.COMPANYID));
				bus.setServiceId(Utility.getPropertyValue(service, Constants.SERVICEID));
				bus.setServiceCategoryName(Utility.getPropertyValue(service, Constants.ROUTENAME));
				bus.setSeater(Boolean.parseBoolean(Utility.getPropertyValue(service, Constants.HASSEATER)));
				bus.setSleeper(Boolean.parseBoolean(Utility.getPropertyValue(service, Constants.HASLEEPER)));
				bus.setDepartureDateTime(getDateTime(Utility.getPropertyValue(service, Constants.DEPARTURETIME)));
				bus.setArrivalDateTime(getDateTime(Utility.getPropertyValue(service, Constants.ARRIVALTIME)));
				bus.setRouteId(Utility.getPropertyValue(service, Constants.ROUTECODE));
				bus.setAc(Boolean.parseBoolean(Utility.getPropertyValue(service, Constants.ISAC)));
				
				/** Fare */
				List<Double> fareList = calculateBusFare(service);
				if(Utility.isEmptyList(fareList))
					continue;
				else
					bus.setFare(fareList);
				
				/** Operator from Aerospike */
				Operator operator = aerospikeManager.getOperatorById(bus.getOperatorId());
				
				/** If operator is disabled, ignore that bus */
				if(operator==null || (operator!=null && !operator.isEnabled()))
					continue;
				
				/** Cancellation Policies */
				CancellationPolicy cancellationPolicy = cancellationPoliciesMap.get(bus.getOperatorId());
				if(cancellationPolicy==null) {
					LOGGER.error("No cancellation policy found for opeartor id ="+bus.getOperatorId()+" and journey date="+request.getJourneyDate());
					continue;
				}
				final String policy = cancellationPolicy.getCancellationPolicy();
				List<String> rawCancellationPolicies = new ArrayList<>();
				rawCancellationPolicies.add(policy);
				bus.setRawCancellationPolicy(rawCancellationPolicies);
				bus.setCancellationPolicy(Utility.createNewCancellationPolicy(policy, Collections.min(bus.getFare())));
				bus.setOldCancellationPolicy(Utility.createCancellationPolicy(policy));
				
				bus.setRefundable(!Boolean.parseBoolean(Utility.getPropertyValue(service, Constants.ISNONREFUNDABLE)));
				bus.setTaxInclusive(Utility.getPropertyValue(service, Constants.HASTAX).equals("1") ? true : false);
			
				String[] routeIds = Utility.getPropertyValue(service, Constants.ROUTECODE).split("~");
				String routeCode = routeIds[0] + "|" + routeIds[1] + "|" + routeIds[2] + "|" + routeIds[3];
				//JSONObject jsonbpdp = getBPDPpoints(Utility.getPropertyValue(service, Constants.ROUTECODE));
				bus.setBoardingPoints(getBoardingPoints( bpdpPointsmap ,routeCode+"|"+"BP"));
				bus.setDroppingPoints(getDroppingPoints( bpdpPointsmap ,routeCode+"|"+"DP"));
				Info info= getInfoDetails(bus, request);
				bus.setInfo(info);
				busList.add(bus);
			}
			searchResponse.setBody(busList);
			if(Utility.isNotEmptyList(busList))
				searchResponse.setStatusCode(Constants.STATUS_CODE_200);
			else
				searchResponse.setStatusCode(Constants.STATUS_CODE_417);
		}catch(Exception exception) {
			LOGGER.error("Exception occurred in transformSearchresponse() ",exception);
		}
		return searchResponse;
	}

	/**
	 * returns fare
	 * @param bus object
	 * @return
	 */
	private List<Double> calculateBusFare(JSONObject service) {
		ArrayList<Double> fare = new ArrayList<>();
		if(Boolean.parseBoolean(Utility.getPropertyValue(service, Constants.HASSEATER))) {
			fare.add(Double.parseDouble(Utility.getPropertyValue(service, Constants.LOWESTSEATERFARE)));							
		}else {
			fare.add(Double.parseDouble(Utility.getPropertyValue(service, Constants.LOWESTSLEEPERFARE)));
		}
		return fare;
	}

	/**
	 * returns arriaveDateTime or departuredateTime
	 * @param dateTime
	 * @return
	 */
	private String getDateTime(String propertyValue) {
		String[] dateTime = propertyValue.split(" ");
		String depTime = Utility.convertTo24HoursFormat(dateTime[1]+" "+dateTime[2]);
		return dateTime[0]+" "+depTime;
	}
	/**
	 * returns duration of bus
	 * @param start dateTime and endTime
	 */
	private String getPaytmDuration(String startTime, String endTime) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		String duration = null;
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(startTime);
			d2 = format.parse(endTime);
			long diff = d2.getTime() - d1.getTime();
			String diffMinutes = String.valueOf(diff / (60 * 1000) % 60);
			String diffHours = String.valueOf(diff / (60 * 60 * 1000) % 24);
			long diffDays = diff / (24 * 60 * 60 * 1000);
			if (diffMinutes.length() != 2) {
				diffMinutes = 0 + "" + diffMinutes;
			}
			if (diffHours.length() != 2) {
				diffHours = 0 + "" + diffHours;
			}
			/** calculation of duration */
			duration = diffDays > 0 ? (24 * 1 + diffHours) + "" + diffMinutes : diffHours + "" + diffMinutes;

		} catch (Exception e) {
			LOGGER.error("Error occurred in getPaytmDuration()"+e.getMessage());
		}
		return duration;
	}
	
	/**
	 * return Info details
	 * @param bus
	 * @param request
	 * @return
	 */
	private Info getInfoDetails(Bus bus, SearchV2Request request) {
		Info info = new Info();
		info.setScheduleId(bus.getTripId());
		info.setScheduleName(bus.getBusName());
		info.setSourceStationId(request.getSourceId());
		info.setDestinationStationId(request.getDestinationId());
		info.setOperatorServiceName(bus.getBusName());
		info.setScheduleNumber(bus.getServiceId());
		info.setRouteId(bus.getRouteId());
		info.setTravelId(bus.getOperatorId());
		info.setBusType(bus.getBusTypeName());
		info.setAvailableSeats(String.valueOf(bus.getAvailableSeats()));
		info.setTripId(bus.getTripId());
		info.setJourneyDate(request.getJourneyDate());
		info.setFare(bus.getFare());
		
		final String cancellationPolicies = bus.getRawCancellationPolicy().get(0);
		info.setCancellationPolicyJSON(Utility.createCancellationPolicy(cancellationPolicies));
		info.setNewCancellationPolicy(Utility.createNewCancellationPolicy(cancellationPolicies, Collections.min(bus.getFare())));
		info.setCancellationPolicyNoteText("Refund amount shown above is indicative "+ "and computed on seat price of " + Collections.min(bus.getFare()));

		return info;
	}
	
	/**
	 * return boardingpoints
	 * @param bpdpmap
	 * @param routeCode
	 * @return
	 */
	private List<StageDetail> getBoardingPoints(HashMap<String, String> bpdpPointsmap, String string) {
		String points = bpdpPointsmap.get(string);
		List<StageDetail> stageList = new ArrayList<>();
		if (points!= null && !points.isEmpty()) {
			JSONArray bpdpJsonArray = new JSONArray(points);
			for (int index = 0; index < bpdpJsonArray.length(); index++) {
				JSONObject pointsObject = bpdpJsonArray.getJSONObject(index);
				StageDetail stages = new StageDetail();
				stages.setId(Utility.getPropertyValue(pointsObject, Constants.ID));
				stages.setAddress(Utility.getPropertyValue(pointsObject, Constants.ADDRESS));
				stages.setDateTime(Utility.getPropertyValue(pointsObject, Constants.DATETIME));
				if (!Utility.getPropertyValue(pointsObject, Constants.CONTACTNUMBER).isEmpty()) {
					stages.setContactNumber(Arrays.asList(Utility.getPropertyValue(pointsObject, Constants.CONTACTNUMBER).trim()
							.split(",")));
				}
				stages.setLandmark(Utility.getPropertyValue(pointsObject, Constants.LANDMARK));
				stages.setName(Utility.getPropertyValue(pointsObject, Constants.NAME));
				stageList.add(stages);
			}
			stageList.sort((StageDetail sd1, StageDetail sd2) -> {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime localDateTime1 = LocalDateTime.parse(sd1.getDateTime(), formatter);
				LocalDateTime localDateTime2 = LocalDateTime.parse(sd2.getDateTime(), formatter);

				return localDateTime1.compareTo(localDateTime2);
			});
		}
		return stageList;
	}
	
	/**
	 * return droppingpoints
	 * @param bpdpmap
	 * @param routeCode
	 * @return
	 */
	private List<StageDetail> getDroppingPoints(HashMap<String, String> bpdpPointsmap, String string) {
		String points = bpdpPointsmap.get(string);
		List<StageDetail> stageList = new ArrayList<>();
		if (points !=null && !points.isEmpty()) {
			JSONArray bpdpJsonArray = new JSONArray(points);
			for (int index = 0; index < bpdpJsonArray.length(); index++) {
				JSONObject pointsObject = bpdpJsonArray.getJSONObject(index);
				StageDetail stages = new StageDetail();
				stages.setId(Utility.getPropertyValue(pointsObject, Constants.ID));
				stages.setAddress(Utility.getPropertyValue(pointsObject, Constants.ADDRESS));
				stages.setDateTime(Utility.getPropertyValue(pointsObject, Constants.DATETIME));
				stages.setName(Utility.getPropertyValue(pointsObject, Constants.NAME));
				stageList.add(stages);
			}
			stageList.sort((StageDetail sd1, StageDetail sd2) -> {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime localDateTime1 = LocalDateTime.parse(sd1.getDateTime(), formatter);
				LocalDateTime localDateTime2 = LocalDateTime.parse(sd2.getDateTime(), formatter);

				return localDateTime1.compareTo(localDateTime2);
			});
		}
		return stageList;
	}

}
